[Header]
Data File Name	Z:\Members\Denise\2022 Burocracy\EMBO Practical Course - October2022\GCMS3\Kefir_T20_R3.qgd
Output Date	10/5/2022
Output Time	4:19:36 PM

[MC Peak Table]
# of Peaks	139
Mass	TIC
Peak#	Ret.Time	Proc.From	Proc.To	Mass	Area	Height	A/H	Conc.	Mark	Name	Ret. Index	Area%	Height%	SI	CAS #
1	5.976	5.800	6.270	TIC	470329031	53338600	8.82	9.65	   	D-(-)-Lactic acid, 2TMS derivative		9.65	4.84	96	0-00-0
2	6.654	6.530	6.875	TIC	55079431	9290601	5.93	1.13	   	2-(Methylamino)ethanol, N-trifluoroacetyl		1.13	0.84	78	0-00-0
3	8.098	8.045	8.255	TIC	13757226	3984276	3.45	0.28	   	Oxalic acid, 2TMS derivative		0.28	0.36	84	18294-04-7
4	8.744	8.670	8.905	TIC	21779910	5479414	3.97	0.45	   	Glycerol, 3TMS derivative		0.45	0.50	94	6787-10-6
5	10.067	10.015	10.485	TIC	93258751	33568570	2.78	1.91	S  	Silanol, trimethyl-, phosphate (3:1)		1.91	3.05	91	10497-05-9
6	10.235	10.205	10.265	TIC	239521	146156	1.64	0.00	T  	Silanol, trimethyl-, phosphate (3:1)		0.00	0.01	76	10497-05-9
7	10.309	10.270	10.475	TIC	5659118	1319780	4.29	0.12	T  	Urea, 2TMS derivative		0.12	0.12	87	18297-63-7
8	10.877	10.835	10.980	TIC	23383521	8733414	2.68	0.48	   	Butanedioic acid, 2TMS derivative		0.48	0.79	95	40309-57-7
9	11.012	10.980	11.090	TIC	2889951	965720	2.99	0.06	 V 	L-Valine, 2TBDMS derivative		0.06	0.09	66	107715-89-9
10	11.521	11.475	11.610	TIC	1985362	623263	3.19	0.04	   	Butanoic acid, 3,4-bis[(trimethylsilyl)oxy]-, trimethylsilyl ester		0.04	0.06	83	55191-53-2
11	12.640	12.605	12.695	TIC	794945	363082	2.19	0.02	   	Glyoxylic oxime acid, bis(trimethylsilyl)-		0.02	0.03	79	51621-77-3
12	12.939	12.895	13.150	TIC	12114233	2092369	5.79	0.25	   	1-Dodecanol, TMS derivative		0.25	0.19	83	6221-88-1
13	13.438	13.395	13.540	TIC	2436113	750684	3.25	0.05	   	2,6-Bis(tert-butyl)phenol, TMS derivative		0.05	0.07	75	10416-73-6
14	13.764	13.720	13.835	TIC	1948822	626027	3.11	0.04	   	Pentanedioic acid, 2-[(trimethylsilyl)oxy]-, bis(trimethylsilyl) ester		0.04	0.06	82	55530-62-6
15	13.927	13.835	13.985	TIC	1716188	334833	5.13	0.04	 V 	D-(+)-Arabitol, 5TMS derivative		0.04	0.03	81	0-00-0
16	14.014	13.985	14.080	TIC	1625230	736660	2.21	0.03	 V 	D-Arabinose, tetrakis(trimethylsilyl) ether, ethyloxime (isomer 2)		0.03	0.07	88	0-00-0
17	14.233	14.190	14.300	TIC	1189451	470288	2.53	0.02	   	D-(-)-Erythrose, tris(trimethylsilyl) ether, ethyloxime (isomer 1)		0.02	0.04	79	0-00-0
18	14.446	14.395	14.525	TIC	4189724	1392077	3.01	0.09	   	L-5-Oxoproline, , 2TMS derivative		0.09	0.13	83	30274-77-2
19	14.550	14.525	14.595	TIC	418948	183883	2.28	0.01	 V 	2,3-Butanediol, O-(trimethylsilyl)-, monoacetate		0.01	0.02	76	0-00-0
20	14.687	14.630	14.735	TIC	1062529	336930	3.15	0.02	   	1-Buten-3-yne		0.02	0.03	71	689-97-4
21	14.760	14.735	14.820	TIC	792075	237545	3.33	0.02	 V 	1-Buten-3-yne		0.02	0.02	76	689-97-4
22	14.856	14.820	14.885	TIC	1425556	490170	2.91	0.03	 V 	Ribonic acid, 2,3,4,5-tetrakis-O-(trimethylsilyl)-, trimethylsilyl ester		0.03	0.04	74	57197-35-0
23	14.962	14.885	15.010	TIC	3282827	800878	4.10	0.07	 V 	Tris(trimethylsiloxy)ethylene		0.07	0.07	79	69097-20-7
24	15.097	15.010	15.150	TIC	2306417	574575	4.01	0.05	 V 	L-Proline, 2TMS derivative		0.05	0.05	73	7364-47-8
25	15.443	15.415	15.515	TIC	532475	219750	2.42	0.01	   	Phosphoric acid, 2-(trimethylsiloxy)-1-[(trimethylsiloxy)methyl]ethyl bis(trimethylsilyl) ester		0.01	0.02	72	31038-12-7
26	15.654	15.615	15.680	TIC	6604905	2658206	2.48	0.14	   	n-Heptyl acrylate		0.14	0.24	85	2499-58-3
27	15.695	15.680	15.825	TIC	13318034	2494444	5.34	0.27	 V 	Pentanoic acid, 2-hydroxy-, ethyl ester		0.27	0.23	75	6938-26-7
28	15.864	15.825	15.935	TIC	14922412	5530159	2.70	0.31	 V 	Phosphoric acid, bis(trimethylsilyl) 2,3-bis[(trimethylsilyl)oxy]propyl ester		0.31	0.50	86	31038-11-6
29	16.009	15.935	16.070	TIC	45621918	19735937	2.31	0.94	 V 	D-Allose, pentakis(trimethylsilyl) ether, methyloxime (syn)		0.94	1.79	94	0-00-0
30	16.102	16.070	16.140	TIC	6068043	2608817	2.33	0.12	 V 	D-(+)-Talose, pentakis(trimethylsilyl) ether, methyloxime (syn)		0.12	0.24	92	0-00-0
31	16.239	16.140	16.390	TIC	51357405	16749351	3.07	1.05	 V 	Citric acid, 4TMS derivative		1.05	1.52	89	14330-97-3
32	16.449	16.390	16.495	TIC	759285	246323	3.08	0.02	 V 	Pentaric acid, 2,3-dideoxy-4-O-(trimethylsilyl)-3-[[(trimethylsilyl)oxy]carbonyl]-, bis(trimethylsilyl) ester		0.02	0.02	81	55517-57-2
33	16.622	16.565	16.700	TIC	3930463	1604386	2.45	0.08	   	Ethyl .alpha.-D-glucopyranoside, 4TMS derivative		0.08	0.15	90	0-00-0
34	16.780	16.700	16.860	TIC	5121985	1889572	2.71	0.11	 V 	Deoxyglucose, 4TMS derivative		0.11	0.17	78	0-00-0
35	16.905	16.860	16.985	TIC	15824815	7743104	2.04	0.32	 V 	d-Erythrotetrofuranose, tris-O-(trimethylsilyl)-		0.32	0.70	80	0-00-0
36	17.470	17.425	17.565	TIC	11798003	5631290	2.10	0.24	   	Myo-Inositol, 6TMS derivative		0.24	0.51	92	2582-79-8
37	17.613	17.565	17.680	TIC	1257592	549632	2.29	0.03	 V 	Mannonic acid, lactone - 4TMS derivative		0.03	0.05	87	55515-28-1
38	18.045	17.985	18.100	TIC	1412345	450157	3.14	0.03	   	D-Allose, oxime (isomer 1), 6TMS derivative		0.03	0.04	83	0-00-0
39	18.305	18.275	18.345	TIC	483700	254438	1.90	0.01	   	Pantothenic acid tritms		0.01	0.02	78	0-00-0
40	18.412	18.345	18.540	TIC	21355727	5372238	3.98	0.44	 V 	Palmitic Acid, TMS derivative		0.44	0.49	87	55520-89-3
41	18.575	18.540	18.630	TIC	2474192	598050	4.14	0.05	 V 	Palmitic Acid, TMS derivative		0.05	0.05	66	55520-89-3
42	18.635	18.630	18.665	TIC	419783	294353	1.43	0.01	 V 	4-Amino-3-chlorobenzonitrile		0.01	0.03	56	21803-75-8
43	18.725	18.665	18.750	TIC	920397	265464	3.47	0.02	 V 	1-Methoxy-2-propanol, TMS derivative		0.02	0.02	64	0-00-0
44	18.773	18.750	18.805	TIC	1280676	552678	2.32	0.03	 V 	2,3,4,5-Tetrahydroxypentanoic acid-1,4-lactone, tris(trimethylsilyl)-		0.03	0.05	64	0-00-0
45	18.936	18.805	18.990	TIC	7143244	2303919	3.10	0.15	 V 	N-Acetyl-D-glucosamine, tetrakis(trimethylsilyl) ether, methyloxime (syn)		0.15	0.21	87	0-00-0
46	19.024	18.990	19.060	TIC	2082509	733588	2.84	0.04	 V 	N-Acetyl-D-glucosamine, tetrakis(trimethylsilyl) ether, methyloxime (syn)		0.04	0.07	77	0-00-0
47	19.075	19.060	19.105	TIC	691972	328510	2.11	0.01	 V 	1-Propanol, TMS derivative		0.01	0.03	65	1825-63-4
48	19.214	19.105	19.270	TIC	8641427	2963587	2.92	0.18	 V 	N-Acetyl-D-glucosamine, tetrakis(trimethylsilyl) ether, methyloxime (syn)		0.18	0.27	84	0-00-0
49	19.292	19.270	19.390	TIC	2910150	768875	3.78	0.06	 V 	Xylose, 4TMS derivative		0.06	0.07	71	0-00-0
50	19.486	19.390	19.615	TIC	14791322	3378165	4.38	0.30	 V 	Propanoic acid, 3-mercapto-, dodecyl ester		0.30	0.31	81	6380-71-8
51	19.660	19.615	19.690	TIC	2202622	550651	4.00	0.05	 V 	Ether, bis[2-(ethylthio)ethyl]		0.05	0.05	76	5648-30-6
52	19.720	19.690	19.790	TIC	1710758	496811	3.44	0.04	 V 	Phosphorimidic acid, N-methoxy-, 4-oxo-2,3-bis[(trimethylsilyl)oxy]butyl bis(trimethylsilyl) ester, [R-(R*,R*)]-		0.04	0.05	60	55517-61-8
53	19.863	19.790	19.940	TIC	2651584	807946	3.28	0.05	 V 	Glyceryl-glycoside TMS ether		0.05	0.07	89	0-00-0
54	20.127	20.080	20.345	TIC	39931714	12967119	3.08	0.82	   	Stearic acid, TMS derivative		0.82	1.18	91	18748-91-9
55	20.634	20.600	20.700	TIC	1166989	484506	2.41	0.02	   	d-Glucose, 2,3,4,5-tetrakis-O-(trimethylsilyl)-, o-methyloxime, 6-[bis(trimethylsilyl) phosphate]		0.02	0.04	74	55530-73-9
56	20.890	20.870	20.935	TIC	480925	161243	2.98	0.01	   	Methyl galactoside, 4TMS derivative		0.01	0.01	66	0-00-0
57	20.966	20.935	21.050	TIC	2665535	804316	3.31	0.05	 V 	D-(+)-Cellobiose, octakis(trimethylsilyl) ether, methyloxime (isomer 2)		0.05	0.07	81	0-00-0
58	21.305	21.255	21.360	TIC	1313379	475089	2.76	0.03	   	D-(+)-Cellobiose, octakis(trimethylsilyl) ether, methyloxime (isomer 2)		0.03	0.04	78	0-00-0
59	21.411	21.360	21.470	TIC	2535677	1053093	2.41	0.05	 V 	Galactitol, 2-amino-2-deoxy-, N-Acetyl-, pentakis(trimethylsilyl) ether		0.05	0.10	67	0-00-0
60	21.813	21.785	21.860	TIC	536820	229808	2.34	0.01	   	1-Methoxy-2-propanol, TBDMS derivative		0.01	0.02	70	0-00-0
61	22.454	22.275	22.485	TIC	1615872627	277899190	5.81	33.14	   	D-Lactose, octakis(trimethylsilyl) ether, methyloxime (isomer 1)		33.14	25.22	69	0-00-0
62	22.546	22.485	22.605	TIC	745227480	189020248	3.94	15.28	 V 	D-Lactose, octakis(trimethylsilyl) ether, methyloxime (isomer 1)		15.28	17.15	76	0-00-0
63	22.621	22.605	22.645	TIC	81254354	38133620	2.13	1.67	 V 	Maltose, octakis(trimethylsilyl) ether, methyloxime (isomer 1)		1.67	3.46	91	0-00-0
64	22.655	22.645	22.755	TIC	117670285	29801672	3.95	2.41	 V 	Maltose, octakis(trimethylsilyl) ether, methyloxime (isomer 1)		2.41	2.70	90	0-00-0
65	22.765	22.755	22.830	TIC	46650144	12723636	3.67	0.96	 V 	D-(+)-Cellobiose, octakis(trimethylsilyl) ether, methyloxime (isomer 1)		0.96	1.15	90	0-00-0
66	22.840	22.830	22.855	TIC	12368581	8264674	1.50	0.25	 V 	D-(+)-Cellobiose, octakis(trimethylsilyl) ether, methyloxime (isomer 1)		0.25	0.75	90	0-00-0
67	22.879	22.855	22.970	TIC	55074949	9673002	5.69	1.13	 V 	Maltose, octakis(trimethylsilyl) ether, methyloxime (isomer 1)		1.13	0.88	90	0-00-0
68	22.985	22.970	23.000	TIC	12244105	6877219	1.78	0.25	 V 	3-.alpha.-Mannobiose, octakis(trimethylsilyl) ether (isomer 2)		0.25	0.62	87	0-00-0
69	23.046	23.000	23.075	TIC	38020405	10440123	3.64	0.78	 V 	2-.alpha.-Mannobiose, octakis(trimethylsilyl) ether (isomer 1)		0.78	0.95	89	0-00-0
70	23.097	23.075	23.130	TIC	27068898	8561946	3.16	0.56	 V 	3-.alpha.-Mannobiose, octakis(trimethylsilyl) ether (isomer 2)		0.56	0.78	87	0-00-0
71	23.145	23.130	23.155	TIC	11905938	7977422	1.49	0.24	 V 	3-.alpha.-Mannobiose, octakis(trimethylsilyl) ether (isomer 2)		0.24	0.72	87	0-00-0
72	23.217	23.155	23.285	TIC	141548413	42766527	3.31	2.90	 V 	Maltose, octakis(trimethylsilyl) ether, methyloxime (isomer 1)		2.90	3.88	89	0-00-0
73	23.322	23.285	23.390	TIC	85702286	23087508	3.71	1.76	 V 	D-(+)-Cellobiose, octakis(trimethylsilyl) ether, methyloxime (isomer 2)		1.76	2.09	88	0-00-0
74	23.425	23.390	23.450	TIC	30310392	8555284	3.54	0.62	 V 	D-(+)-Cellobiose, octakis(trimethylsilyl) ether, methyloxime (isomer 2)		0.62	0.78	86	0-00-0
75	23.460	23.450	23.480	TIC	15746439	8826952	1.78	0.32	 V 	D-(+)-Cellobiose, octakis(trimethylsilyl) ether, methyloxime (isomer 2)		0.32	0.80	87	0-00-0
76	23.557	23.480	23.655	TIC	120123399	18149167	6.62	2.46	 V 	D-(+)-Cellobiose, octakis(trimethylsilyl) ether, methyloxime (isomer 1)		2.46	1.65	89	0-00-0
77	23.687	23.655	23.715	TIC	35513767	10530977	3.37	0.73	 V 	D-(+)-Cellobiose, octakis(trimethylsilyl) ether, methyloxime (isomer 2)		0.73	0.96	88	0-00-0
78	23.725	23.715	23.850	TIC	66811397	9343680	7.15	1.37	 V 	D-(+)-Cellobiose, octakis(trimethylsilyl) ether, methyloxime (isomer 2)		1.37	0.85	87	0-00-0
79	23.883	23.850	24.030	TIC	65222739	11389323	5.73	1.34	 V 	3-.alpha.-Mannobiose, octakis(trimethylsilyl) ether (isomer 2)		1.34	1.03	88	0-00-0
80	24.063	24.030	24.125	TIC	15936199	5979975	2.66	0.33	 V 	D-Lactose, octakis(trimethylsilyl) ether, methyloxime (isomer 2)		0.33	0.54	88	0-00-0
81	24.168	24.125	24.235	TIC	4784320	1992478	2.40	0.10	 V 	Galactinol, nonakis(trimethylsilyl) ether		0.10	0.18	94	0-00-0
82	24.335	24.315	24.405	TIC	255328	108979	2.34	0.01	   	Maltose, octakis(trimethylsilyl) ether, methyloxime (isomer 2)		0.01	0.01	73	0-00-0
83	24.462	24.405	24.500	TIC	1024849	330565	3.10	0.02	   	D-(+)-Cellobiose, (isomer 1), 8TMS derivative		0.02	0.03	83	0-00-0
84	24.670	24.645	24.715	TIC	260886	155336	1.68	0.01	   	D-Mannopyranose, 5TMS derivative		0.01	0.01	64	55529-69-6
85	24.740	24.715	24.795	TIC	1407857	322551	4.36	0.03	 V 	2-Methyl-1,2-bis(trimethylsilyloxy)butane		0.03	0.03	66	0-00-0
86	24.839	24.795	24.880	TIC	2608459	819482	3.18	0.05	 V 	Maltose, octakis(trimethylsilyl) ether, methyloxime (isomer 2)		0.05	0.07	74	0-00-0
87	24.915	24.880	24.995	TIC	630972	232400	2.72	0.01	 V 	Octadecanamide, N-(2-methylpropyl)-N-nitroso-		0.01	0.02	63	74420-93-2
88	25.005	24.995	25.090	TIC	146219	69804	2.09	0.00	   	D-(+)-Cellobiose, (isomer 1), 8TMS derivative		0.00	0.01	59	0-00-0
89	25.325	25.245	25.375	TIC	1876598	385643	4.87	0.04	   	.beta.-Gentiobiose, octakis(trimethylsilyl) ether, methyloxime (isomer 1)		0.04	0.03	39	0-00-0
90	25.464	25.375	25.515	TIC	2963423	874184	3.39	0.06	   	Heneicosane		0.06	0.08	79	629-94-7
91	25.545	25.515	25.560	TIC	389789	144553	2.70	0.01	 V 	l-Leucine, N-(4-methylbenzoyl)-, dodecyl ester		0.01	0.01	40	0-00-0
92	25.936	25.875	25.965	TIC	1599893	466838	3.43	0.03	   	Octadecane, 2,6,10,14-tetramethyl-		0.03	0.04	55	54964-82-8
93	26.034	25.965	26.055	TIC	1752045	434274	4.03	0.04	 V 	Methyl .alpha.-D-glucofuranoside, 4TMS derivative		0.04	0.04	47	6736-96-5
94	26.088	26.055	26.140	TIC	2122604	624157	3.40	0.04	 V 	11-Methyltricosane		0.04	0.06	89	27538-41-6
95	26.160	26.140	26.180	TIC	367483	214737	1.71	0.01	 V 	1-Heptadecanol, 17-bromo-		0.01	0.02	42	338389-27-8
96	26.404	26.360	26.445	TIC	1519874	516325	2.94	0.03	   	Tetrapentacontane, 1,54-dibromo-		0.03	0.05	82	0-00-0
97	26.525	26.445	26.555	TIC	2073039	512678	4.04	0.04	 V 	Tetracontane-1,40-diol		0.04	0.05	77	181719-30-2
98	26.570	26.555	26.595	TIC	1238718	643687	1.92	0.03	 V 	Dotriacontyl pentafluoropropionate		0.03	0.06	66	0-00-0
99	26.688	26.595	26.750	TIC	6497259	1244036	5.22	0.13	 V 	Hexacontane		0.13	0.11	90	7667-80-3
100	26.859	26.750	26.885	TIC	2791573	481962	5.79	0.06	 V 	[1,1'-Bicyclohexyl]-4-carboxylic acid, 4'-pentyl-, 4-fluorophenyl ester		0.06	0.04	67	88878-50-6
101	26.925	26.885	26.970	TIC	2305470	503330	4.58	0.05	 V 	[1,1'-Bicyclohexyl]-4-carboxylic acid, 4'-pentyl-, 4-fluorophenyl ester		0.05	0.05	71	88878-50-6
102	26.995	26.970	27.030	TIC	2642327	797916	3.31	0.05	 V 	erythro-7,8-Bromochlorodisparlure		0.05	0.07	78	0-00-0
103	27.086	27.030	27.195	TIC	9066252	1233124	7.35	0.19	 V 	Tetrapentacontane, 1,54-dibromo-		0.19	0.11	84	0-00-0
104	27.215	27.195	27.230	TIC	1277368	638292	2.00	0.03	 V 	Tetracontane-1,40-diol		0.03	0.06	75	181719-30-2
105	27.329	27.230	27.395	TIC	12617534	1837128	6.87	0.26	 V 	Hexacontane		0.26	0.17	88	7667-80-3
106	27.460	27.395	27.485	TIC	4127909	805245	5.13	0.08	 V 	Tetracontane-1,40-diol		0.08	0.07	69	181719-30-2
107	27.560	27.485	27.600	TIC	8546643	2432020	3.51	0.18	 V 	Cholest-5-en-3-ol, (3.alpha.)-, TMS derivative		0.18	0.22	77	16134-40-0
108	27.610	27.600	27.640	TIC	2570314	1103088	2.33	0.05	 V 	Tetracontane-1,40-diol		0.05	0.10	71	181719-30-2
109	27.655	27.640	27.675	TIC	2418383	1175781	2.06	0.05	 V 	erythro-7,8-Bromochlorodisparlure		0.05	0.11	74	0-00-0
110	27.720	27.675	27.735	TIC	4955898	1493703	3.32	0.10	 V 	erythro-7,8-Bromochlorodisparlure		0.10	0.14	72	0-00-0
111	27.767	27.735	27.835	TIC	13583161	3219846	4.22	0.28	 V 	Per-O-trimethylsilyl-(3-O-.beta.-d-mannopyranosyl-d-glucitol)		0.28	0.29	59	0-00-0
112	27.885	27.835	27.925	TIC	11871464	2932670	4.05	0.24	 V 	erythro-7,8-Bromochlorodisparlure		0.24	0.27	59	0-00-0
113	27.961	27.925	28.020	TIC	13872293	3081348	4.50	0.28	 V 	Tetrapentacontane, 1,54-dibromo-		0.28	0.28	67	0-00-0
114	28.081	28.020	29.390	TIC	171175671	49402011	3.46	3.51	SV 	L-Threitol, 4TMS derivative		3.51	4.48	61	0-00-0
115	28.225	28.210	28.240	TIC	269523	178012	1.51	0.01	T  	erythro-7,8-Bromochlorodisparlure		0.01	0.02	69	0-00-0
116	28.300	28.240	28.330	TIC	1754927	426381	4.12	0.04	TV 	erythro-7,8-Bromochlorodisparlure		0.04	0.04	71	0-00-0
117	28.410	28.330	28.470	TIC	2176510	453977	4.79	0.04	TV 	erythro-7,8-Bromochlorodisparlure		0.04	0.04	73	0-00-0
118	28.538	28.470	28.585	TIC	1241488	266429	4.66	0.03	TV 	erythro-7,8-Bromochlorodisparlure		0.03	0.02	68	0-00-0
119	28.605	28.585	28.620	TIC	151763	105758	1.44	0.00	TV 	[1,1'-Bicyclohexyl]-4-carboxylic acid, 4'-pentyl-, 4-fluorophenyl ester		0.00	0.01	64	88878-50-6
120	28.718	28.625	28.740	TIC	1558210	440948	3.53	0.03	T  	erythro-7,8-Bromochlorodisparlure		0.03	0.04	66	0-00-0
121	28.788	28.740	28.805	TIC	2621951	970639	2.70	0.05	TV 	erythro-7,8-Bromochlorodisparlure		0.05	0.09	68	0-00-0
122	28.839	28.805	28.885	TIC	3230684	905563	3.57	0.07	TV 	erythro-7,8-Bromochlorodisparlure		0.07	0.08	67	0-00-0
123	28.989	28.885	29.035	TIC	6402647	1174248	5.45	0.13	TV 	erythro-7,8-Bromochlorodisparlure		0.13	0.11	61	0-00-0
124	29.050	29.035	29.080	TIC	1372462	606068	2.26	0.03	TV 	2-Dodecen-1-yl(-)succinic anhydride		0.03	0.05	62	19780-11-1
125	29.135	29.080	29.165	TIC	1576619	395755	3.98	0.03	TV 	[1,1'-Bicyclohexyl]-4-carboxylic acid, 4'-pentyl-, 4-fluorophenyl ester		0.03	0.04	59	88878-50-6
126	29.210	29.165	29.295	TIC	1232439	265961	4.63	0.03	TV 	[1,1'-Bicyclohexyl]-4-carboxylic acid, 4'-pentyl-, 4-fluorophenyl ester		0.03	0.02	57	88878-50-6
127	29.310	29.295	29.390	TIC	375438	165106	2.27	0.01	T  	Cyclohexane, 1-(1,5-dimethylhexyl)-4-(4-methylpentyl)-		0.01	0.01	57	56009-20-2
128	29.437	29.390	29.530	TIC	25572613	9990549	2.56	0.52	 V 	Phenol, 2,4-bis(1,1-dimethylethyl)-, phosphite (3:1)		0.52	0.91	81	31570-04-4
129	29.563	29.530	29.610	TIC	1740767	627554	2.77	0.04	 V 	D-Homo-24-nor-17-oxachola-20,22-diene-3,16-dione, 1,2:14,15:21,23-triepoxy-7-hydroxy-4,4,8-trimethyl-, (5.alpha.,7.alpha.,13.al		0.04	0.06	54	35963-08-7
130	29.760	29.740	29.845	TIC	654055	183854	3.56	0.01	   	Cyclotrisiloxane, 2,4,6-trimethyl-2,4,6-triphenyl-		0.01	0.02	59	546-45-2
131	29.913	29.845	29.975	TIC	3503310	807099	4.34	0.07	 V 	Cyclotrisiloxane, 2,4,6-trimethyl-2,4,6-triphenyl-		0.07	0.07	52	546-45-2
132	29.995	29.975	30.040	TIC	468991	228183	2.06	0.01	 V 	Cyclotrisiloxane, 2,4,6-trimethyl-2,4,6-triphenyl-		0.01	0.02	61	546-45-2
133	30.394	30.370	30.475	TIC	610921	219834	2.78	0.01	   	Cyclotrisiloxane, 2,4,6-trimethyl-2,4,6-triphenyl-		0.01	0.02	66	546-45-2
134	30.875	30.860	30.890	TIC	153084	94285	1.62	0.00	   	Cyclotrisiloxane, 2,4,6-trimethyl-2,4,6-triphenyl-		0.00	0.01	72	546-45-2
135	30.964	30.890	31.285	TIC	141508401	29111311	4.86	2.90	SV 	Propanoic acid, 3,3'-thiobis-, didodecyl ester		2.90	2.64	76	123-28-4
136	31.258	31.220	31.275	TIC	434595	204893	2.12	0.01	T  	Cyclotrisiloxane, 2,4,6-trimethyl-2,4,6-triphenyl-		0.01	0.02	54	546-45-2
137	31.339	31.285	31.430	TIC	16109456	3330918	4.84	0.33	 V 	Tris(2,4-di-tert-butylphenyl) phosphate		0.33	0.30	51	95906-11-9
138	31.440	31.430	31.500	TIC	1768592	584974	3.02	0.04	 V 	Cyclotrisiloxane, 2,4,6-trimethyl-2,4,6-triphenyl-		0.04	0.05	66	546-45-2
139	31.555	31.500	31.615	TIC	1576156	305191	5.16	0.03	 V 	Cyclotrisiloxane, 2,4,6-trimethyl-2,4,6-triphenyl-		0.03	0.03	67	546-45-2

