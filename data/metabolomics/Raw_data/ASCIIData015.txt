[Header]
Data File Name	Z:\Members\Denise\2022 Burocracy\EMBO Practical Course - October2022\GCMS3\Kefir_T15_R3.qgd
Output Date	10/5/2022
Output Time	4:19:16 PM

[MC Peak Table]
# of Peaks	141
Mass	TIC
Peak#	Ret.Time	Proc.From	Proc.To	Mass	Area	Height	A/H	Conc.	Mark	Name	Ret. Index	Area%	Height%	SI	CAS #
1	5.894	5.755	6.200	TIC	333472220	50835733	6.56	7.02	   	D-(-)-Lactic acid, 2TMS derivative		7.02	4.89	96	0-00-0
2	6.587	6.495	6.845	TIC	58433359	11832861	4.94	1.23	   	2-(Methylamino)ethanol, N-trifluoroacetyl		1.23	1.14	79	0-00-0
3	8.086	8.040	8.175	TIC	1753164	460285	3.81	0.04	   	Oxalic acid, 2TMS derivative		0.04	0.04	76	18294-04-7
4	8.727	8.665	8.890	TIC	13199649	3517239	3.75	0.28	   	Glycerol, 3TMS derivative		0.28	0.34	94	6787-10-6
5	10.056	10.000	10.250	TIC	96939253	35988737	2.69	2.04	S  	Silanol, trimethyl-, phosphate (3:1)		2.04	3.47	90	10497-05-9
6	10.216	10.195	10.250	TIC	382389	240860	1.59	0.01	T  	Silanol, trimethyl-, phosphate (3:1)		0.01	0.02	76	10497-05-9
7	10.292	10.250	10.495	TIC	20794662	6445991	3.23	0.44	 V 	Urea, 2TMS derivative		0.44	0.62	90	18297-63-7
8	10.874	10.825	10.975	TIC	15949570	5552948	2.87	0.34	   	Butanedioic acid, 2TMS derivative		0.34	0.53	94	40309-57-7
9	11.010	10.975	11.085	TIC	2409072	758067	3.18	0.05	 V 	4-Aminobutanoic acid, 2TMS derivative		0.05	0.07	66	39538-11-9
10	11.521	11.475	11.595	TIC	1808409	593471	3.05	0.04	   	Butanoic acid, 3,4-bis[(trimethylsilyl)oxy]-, trimethylsilyl ester		0.04	0.06	83	55191-53-2
11	12.638	12.600	12.695	TIC	1134091	481039	2.36	0.02	   	Glyoxylic oxime acid, bis(trimethylsilyl)-		0.02	0.05	79	51621-77-3
12	12.946	12.900	13.125	TIC	10752205	1834007	5.86	0.23	   	1-Dodecanol, TMS derivative		0.23	0.18	81	6221-88-1
13	13.436	13.395	13.545	TIC	2345855	726603	3.23	0.05	   	2,6-Bis(tert-butyl)phenol, TMS derivative		0.05	0.07	76	10416-73-6
14	13.767	13.725	13.845	TIC	2545664	798388	3.19	0.05	   	Pentanedioic acid, 2-[(trimethylsilyl)oxy]-, bis(trimethylsilyl) ester		0.05	0.08	78	55530-62-6
15	13.934	13.845	13.985	TIC	1683418	341591	4.93	0.04	 V 	L-Sorbose, pentakis(trimethylsilyl) ether, trimethylsilyloxime (isomer 1)		0.04	0.03	82	0-00-0
16	14.015	13.985	14.075	TIC	1531508	717333	2.14	0.03	 V 	D-(-)-Lyxose, tetrakis(trimethylsilyl) ether, methyloxime (syn)		0.03	0.07	88	0-00-0
17	14.235	14.195	14.290	TIC	968238	438480	2.21	0.02	   	D-(-)-Erythrose, tris(trimethylsilyl) ether, ethyloxime (isomer 1)		0.02	0.04	79	0-00-0
18	14.446	14.395	14.520	TIC	4552985	1520186	3.00	0.10	   	L-5-Oxoproline, , 2TMS derivative		0.10	0.15	85	30274-77-2
19	14.550	14.520	14.585	TIC	425329	172858	2.46	0.01	 V 	2,3-Butanediol, O-(trimethylsilyl)-, monoacetate		0.01	0.02	77	0-00-0
20	14.685	14.630	14.735	TIC	1067167	327914	3.25	0.02	   	Silanol, trimethyl-		0.02	0.03	69	1066-40-6
21	14.760	14.735	14.825	TIC	894015	227345	3.93	0.02	 V 	Silanol, trimethyl-		0.02	0.02	71	1066-40-6
22	14.850	14.825	14.930	TIC	2331935	493927	4.72	0.05	 V 	Ribonic acid, 2,3,4,5-tetrakis-O-(trimethylsilyl)-, trimethylsilyl ester		0.05	0.05	76	57197-35-0
23	14.962	14.930	15.015	TIC	2460362	827809	2.97	0.05	 V 	Ribonic acid, 2,3,4,5-tetrakis-O-(trimethylsilyl)-, trimethylsilyl ester		0.05	0.08	77	57197-35-0
24	15.095	15.015	15.150	TIC	1876537	422706	4.44	0.04	 V 	L-Proline, 2TMS derivative		0.04	0.04	73	7364-47-8
25	15.315	15.285	15.420	TIC	882026	198426	4.45	0.02	   	1-Buten-3-yne		0.02	0.02	79	689-97-4
26	15.445	15.420	15.530	TIC	986727	365556	2.70	0.02	 V 	Phosphoric acid, 2-(trimethylsiloxy)-1-[(trimethylsiloxy)methyl]ethyl bis(trimethylsilyl) ester		0.02	0.04	74	31038-12-7
27	15.580	15.560	15.610	TIC	274556	130295	2.11	0.01	   	Silanol, trimethyl-		0.01	0.01	84	1066-40-6
28	15.647	15.610	15.685	TIC	11643455	3726862	3.12	0.24	 V 	n-Heptyl acrylate		0.24	0.36	86	2499-58-3
29	15.695	15.685	15.730	TIC	6611346	2864914	2.31	0.14	 V 	Pentanoic acid, 2-hydroxy-, ethyl ester		0.14	0.28	73	6938-26-7
30	15.740	15.730	15.810	TIC	7526789	2042302	3.69	0.16	 V 	Pentanoic acid, 2-hydroxy-, ethyl ester		0.16	0.20	73	6938-26-7
31	15.864	15.810	15.935	TIC	21099904	7513218	2.81	0.44	 V 	Phosphoric acid, bis(trimethylsilyl) 2,3-bis[(trimethylsilyl)oxy]propyl ester		0.44	0.72	86	31038-11-6
32	16.011	15.935	16.070	TIC	78491740	34604821	2.27	1.65	 V 	D-(+)-Talose, pentakis(trimethylsilyl) ether, methyloxime (syn)		1.65	3.33	94	0-00-0
33	16.103	16.070	16.145	TIC	23105048	10437195	2.21	0.49	 V 	D-(+)-Talose, pentakis(trimethylsilyl) ether, methyloxime (syn)		0.49	1.00	93	0-00-0
34	16.240	16.145	16.740	TIC	85910221	24612468	3.49	1.81	SV 	Citric acid, 4TMS derivative		1.81	2.37	88	14330-97-3
35	16.444	16.410	16.555	TIC	1057118	377786	2.80	0.02	T  	Pentaric acid, 2,3-dideoxy-4-O-(trimethylsilyl)-3-[[(trimethylsilyl)oxy]carbonyl]-, bis(trimethylsilyl) ester		0.02	0.04	83	55517-57-2
36	16.623	16.565	16.650	TIC	2669042	1144708	2.33	0.06	T  	Ethyl .alpha.-D-glucopyranoside, 4TMS derivative		0.06	0.11	87	0-00-0
37	16.675	16.650	16.730	TIC	873541	338088	2.58	0.02	TV 	Malonic acid, bis(2-trimethylsilylethyl ester		0.02	0.03	68	90744-45-9
38	16.780	16.745	16.865	TIC	6143886	2234403	2.75	0.13	   	Deoxyglucose, 4TMS derivative		0.13	0.22	79	0-00-0
39	16.906	16.865	16.980	TIC	16177210	7969346	2.03	0.34	   	d-Erythrotetrofuranose, tris-O-(trimethylsilyl)-		0.34	0.77	80	0-00-0
40	17.471	17.425	17.565	TIC	11354667	5570080	2.04	0.24	   	Myo-Inositol, 6TMS derivative		0.24	0.54	93	2582-79-8
41	17.616	17.565	17.675	TIC	1717049	702046	2.45	0.04	 V 	Mannonic acid, lactone - 4TMS derivative		0.04	0.07	87	55515-28-1
42	18.046	17.985	18.100	TIC	1378114	429929	3.21	0.03	   	D-Allose, oxime (isomer 1), 6TMS derivative		0.03	0.04	84	0-00-0
43	18.304	18.275	18.335	TIC	395336	227254	1.74	0.01	   	Pantothenic acid tritms		0.01	0.02	80	0-00-0
44	18.409	18.340	18.555	TIC	20277134	5734107	3.54	0.43	   	Palmitic Acid, TMS derivative		0.43	0.55	87	55520-89-3
45	18.730	18.700	18.875	TIC	1109875	190176	5.84	0.02	   	1-Methoxy-2-propanol, TMS derivative		0.02	0.02	67	0-00-0
46	18.936	18.875	18.990	TIC	12088365	4877994	2.48	0.25	 V 	N-Acetyl-D-glucosamine, tetrakis(trimethylsilyl) ether, methyloxime (syn)		0.25	0.47	89	0-00-0
47	19.023	18.990	19.105	TIC	2654348	705054	3.76	0.06	 V 	N-Acetyl-D-glucosamine, tetrakis(trimethylsilyl) ether, methyloxime (syn)		0.06	0.07	78	0-00-0
48	19.145	19.105	19.180	TIC	915907	251826	3.64	0.02	 V 	Hippuric acid, TMS derivative		0.02	0.02	71	2078-24-2
49	19.216	19.180	19.265	TIC	6918900	2787668	2.48	0.15	 V 	N-Acetyl-D-glucosamine, tetrakis(trimethylsilyl) ether, methyloxime (syn)		0.15	0.27	86	0-00-0
50	19.294	19.265	19.325	TIC	2126242	841786	2.53	0.04	 V 	Xylose, 4TMS derivative		0.04	0.08	71	0-00-0
51	19.340	19.325	19.390	TIC	1005338	501311	2.01	0.02	 V 	1-Methoxy-2-propanol, TMS derivative		0.02	0.05	63	0-00-0
52	19.478	19.390	19.625	TIC	18603428	4177194	4.45	0.39	 V 	Propanoic acid, 3-mercapto-, dodecyl ester		0.39	0.40	86	6380-71-8
53	19.655	19.625	19.690	TIC	2036430	637815	3.19	0.04	 V 	Ether, bis[2-(ethylthio)ethyl]		0.04	0.06	77	5648-30-6
54	19.714	19.690	19.770	TIC	1494577	495467	3.02	0.03	 V 	Phosphorimidic acid, N-methoxy-, 4-oxo-2,3-bis[(trimethylsilyl)oxy]butyl bis(trimethylsilyl) ester, [R-(R*,R*)]-		0.03	0.05	60	55517-61-8
55	19.867	19.770	19.940	TIC	1942433	529344	3.67	0.04	 V 	Glyceryl-glycoside TMS ether		0.04	0.05	83	0-00-0
56	20.129	20.080	20.175	TIC	33995715	14226077	2.39	0.72	   	Stearic acid, TMS derivative		0.72	1.37	91	18748-91-9
57	20.186	20.175	20.350	TIC	9108137	3484466	2.61	0.19	 V 	Stearic acid, TMS derivative		0.19	0.34	64	18748-91-9
58	20.633	20.600	20.705	TIC	1489496	655813	2.27	0.03	   	d-Glucose, 2,3,4,5-tetrakis-O-(trimethylsilyl)-, o-methyloxime, 6-[bis(trimethylsilyl) phosphate]		0.03	0.06	74	55530-73-9
59	20.730	20.705	20.875	TIC	757743	175761	4.31	0.02	   	Methanol, TMS derivative		0.02	0.02	71	1825-61-2
60	20.895	20.875	20.935	TIC	639345	236127	2.71	0.01	 V 	Methyl-4-O-methyl-.alpha.-D-glucopyranoside, 3TMS derivative		0.01	0.02	65	52430-40-7
61	20.970	20.935	21.060	TIC	2858058	848670	3.37	0.06	 V 	D-(+)-Cellobiose, octakis(trimethylsilyl) ether, methyloxime (isomer 2)		0.06	0.08	80	0-00-0
62	21.235	21.205	21.265	TIC	603946	222305	2.72	0.01	   	Methionol, TBDMS derivative		0.01	0.02	69	0-00-0
63	21.306	21.265	21.365	TIC	1811089	568311	3.19	0.04	 V 	D-(+)-Cellobiose, octakis(trimethylsilyl) ether, methyloxime (isomer 2)		0.04	0.05	80	0-00-0
64	21.412	21.365	21.465	TIC	2318718	998125	2.32	0.05	 V 	Galactitol, 2-amino-2-deoxy-, N-Acetyl-, pentakis(trimethylsilyl) ether		0.05	0.10	66	0-00-0
65	21.745	21.720	21.790	TIC	369402	124074	2.98	0.01	   	Silane, (2-methoxyethyl)trimethyl-		0.01	0.01	76	18173-63-2
66	21.818	21.790	21.865	TIC	649589	280893	2.31	0.01	 V 	1-Methoxy-2-propanol, TBDMS derivative		0.01	0.03	74	0-00-0
67	22.240	22.210	22.280	TIC	515812	208248	2.48	0.01	   	Silane, (2-methoxyethyl)trimethyl-		0.01	0.02	72	18173-63-2
68	22.450	22.280	22.485	TIC	1565959339	272759564	5.74	32.95	 V 	D-Lactose, octakis(trimethylsilyl) ether, methyloxime (isomer 1)		32.95	26.26	68	0-00-0
69	22.544	22.485	23.120	TIC	1074694793	181896688	5.91	22.61	SV 	D-Lactose, octakis(trimethylsilyl) ether, methyloxime (isomer 1)		22.61	17.51	77	0-00-0
70	22.765	22.750	22.785	TIC	669253	832225	0.80	0.01	T  	D-(+)-Cellobiose, octakis(trimethylsilyl) ether, methyloxime (isomer 1)		0.01	0.08	91	0-00-0
71	22.876	22.835	22.950	TIC	3612334	1321985	2.73	0.08	T  	Maltose, octakis(trimethylsilyl) ether, methyloxime (isomer 1)		0.08	0.13	89	0-00-0
72	22.990	22.980	23.000	TIC	151316	174080	0.87	0.00	T  	3-.alpha.-Mannobiose, octakis(trimethylsilyl) ether (isomer 2)		0.00	0.02	87	0-00-0
73	23.046	23.005	23.075	TIC	5485580	2771268	1.98	0.12	T  	3-.alpha.-Mannobiose, octakis(trimethylsilyl) ether (isomer 1)		0.12	0.27	88	0-00-0
74	23.102	23.075	23.115	TIC	835185	474805	1.76	0.02	TV 	3-.alpha.-Mannobiose, octakis(trimethylsilyl) ether (isomer 2)		0.02	0.05	87	0-00-0
75	23.217	23.120	23.280	TIC	194309161	54551169	3.56	4.09	 V 	Maltose, octakis(trimethylsilyl) ether, methyloxime (isomer 1)		4.09	5.25	89	0-00-0
76	23.321	23.280	23.410	TIC	130085993	32483250	4.00	2.74	 V 	D-(+)-Cellobiose, octakis(trimethylsilyl) ether, methyloxime (isomer 2)		2.74	3.13	87	0-00-0
77	23.440	23.410	23.455	TIC	27940811	10482977	2.67	0.59	 V 	3-.alpha.-Mannobiose, octakis(trimethylsilyl) ether (isomer 1)		0.59	1.01	86	0-00-0
78	23.480	23.455	23.505	TIC	32114531	10844454	2.96	0.68	 V 	D-(+)-Cellobiose, octakis(trimethylsilyl) ether, methyloxime (isomer 2)		0.68	1.04	86	0-00-0
79	23.558	23.505	23.640	TIC	106418134	17398897	6.12	2.24	 V 	D-Lactose, octakis(trimethylsilyl) ether, methyloxime (isomer 2)		2.24	1.68	89	0-00-0
80	23.650	23.640	23.665	TIC	16680630	11131099	1.50	0.35	 V 	3-.alpha.-Mannobiose, octakis(trimethylsilyl) ether (isomer 1)		0.35	1.07	87	0-00-0
81	23.686	23.665	23.720	TIC	37687163	11872196	3.17	0.79	 V 	D-(+)-Cellobiose, octakis(trimethylsilyl) ether, methyloxime (isomer 2)		0.79	1.14	88	0-00-0
82	23.734	23.720	23.850	TIC	77412302	11188384	6.92	1.63	 V 	3-.alpha.-Mannobiose, octakis(trimethylsilyl) ether (isomer 1)		1.63	1.08	88	0-00-0
83	23.884	23.850	24.030	TIC	79522486	13391134	5.94	1.67	 V 	3-.alpha.-Mannobiose, octakis(trimethylsilyl) ether (isomer 2)		1.67	1.29	88	0-00-0
84	24.065	24.030	24.130	TIC	20242509	7347230	2.76	0.43	 V 	D-(+)-Cellobiose, octakis(trimethylsilyl) ether, methyloxime (isomer 2)		0.43	0.71	87	0-00-0
85	24.168	24.130	24.230	TIC	5741697	2147101	2.67	0.12	 V 	Galactinol, nonakis(trimethylsilyl) ether		0.12	0.21	93	0-00-0
86	24.295	24.230	24.310	TIC	775311	110272	7.03	0.02	 V 	D-(+)-Cellobiose, octakis(trimethylsilyl) ether, methyloxime (isomer 2)		0.02	0.01	84	0-00-0
87	24.461	24.430	24.505	TIC	442555	247167	1.79	0.01	   	Lactose, 8TMS derivative		0.01	0.02	81	42390-78-3
88	24.665	24.645	24.715	TIC	524442	204419	2.57	0.01	   	D-Mannopyranose, 5TMS derivative		0.01	0.02	66	55529-69-6
89	24.740	24.715	24.765	TIC	859572	372737	2.31	0.02	 V 	2-Methyl-1,2-bis(trimethylsilyloxy)butane		0.02	0.04	66	0-00-0
90	24.775	24.765	24.800	TIC	819944	399566	2.05	0.02	 V 	2-Methyl-1,2-bis(trimethylsilyloxy)butane		0.02	0.04	66	0-00-0
91	24.836	24.800	24.890	TIC	2543604	812408	3.13	0.05	 V 	Maltose, octakis(trimethylsilyl) ether, methyloxime (isomer 2)		0.05	0.08	75	0-00-0
92	24.909	24.890	24.955	TIC	429682	240138	1.79	0.01	 V 	2-Amino-2-deoxyhexose, 5TMS derivative		0.01	0.02	64	56248-46-5
93	25.105	25.085	25.140	TIC	392409	182009	2.16	0.01	   	D-(+)-Cellobiose, octakis(trimethylsilyl) ether, methyloxime (isomer 2)		0.01	0.02	69	0-00-0
94	25.158	25.140	25.185	TIC	390846	232363	1.68	0.01	 V 	D-(+)-Cellobiose, octakis(trimethylsilyl) ether, methyloxime (isomer 1)		0.01	0.02	54	0-00-0
95	25.320	25.185	25.375	TIC	1985114	448466	4.43	0.04	 V 	D-Lactose, octakis(trimethylsilyl) ether, methyloxime (isomer 1)		0.04	0.04	37	0-00-0
96	25.467	25.375	25.525	TIC	3523291	944855	3.73	0.07	 V 	Squalane		0.07	0.09	75	111-01-3
97	25.545	25.525	25.580	TIC	481133	219504	2.19	0.01	 V 	L-(+)-Rhamnopyranose, 4TMS derivative		0.01	0.02	60	0-00-0
98	25.942	25.880	25.970	TIC	1399937	454872	3.08	0.03	   	Eicosane, 2,6,10,14,18-pentamethyl-		0.03	0.04	53	51794-16-2
99	26.030	25.970	26.060	TIC	1561864	409847	3.81	0.03	 V 	Ethyl 2-ethyl-3-methyl-3-trimethylsilyloxyvalerate		0.03	0.04	47	0-00-0
100	26.093	26.060	26.140	TIC	2004180	624400	3.21	0.04	 V 	11-Methyltricosane		0.04	0.06	89	27538-41-6
101	26.165	26.140	26.175	TIC	344447	163401	2.11	0.01	 V 	1-Heptadecanol, 17-bromo-		0.01	0.02	47	338389-27-8
102	26.410	26.365	26.490	TIC	1766297	447669	3.95	0.04	   	Hexacontane		0.04	0.04	81	7667-80-3
103	26.515	26.490	26.550	TIC	1075111	373015	2.88	0.02	 V 	erythro-7,8-Bromochlorodisparlure		0.02	0.04	74	0-00-0
104	26.575	26.550	26.600	TIC	1219586	548784	2.22	0.03	 V 	Dotriacontyl pentafluoropropionate		0.03	0.05	63	0-00-0
105	26.689	26.600	26.745	TIC	4745934	1110246	4.27	0.10	 V 	Hexacontane		0.10	0.11	90	7667-80-3
106	26.750	26.745	26.770	TIC	184043	264229	0.70	0.00	 V 	erythro-7,8-Bromochlorodisparlure		0.00	0.03	78	0-00-0
107	26.940	26.905	26.955	TIC	355617	153488	2.32	0.01	   	[1,1'-Bicyclohexyl]-4-carboxylic acid, 4'-pentyl-, 4-fluorophenyl ester		0.01	0.01	69	88878-50-6
108	26.980	26.955	27.040	TIC	1862084	413370	4.50	0.04	 V 	Tetracontane-1,40-diol		0.04	0.04	77	181719-30-2
109	27.094	27.040	27.170	TIC	4554999	814745	5.59	0.10	 V 	Tetrapentacontane, 1,54-dibromo-		0.10	0.08	86	0-00-0
110	27.175	27.170	27.190	TIC	120221	204546	0.59	0.00	 V 	Tetracontane-1,40-diol		0.00	0.02	74	181719-30-2
111	27.330	27.190	27.395	TIC	6604746	1171516	5.64	0.14	   	Hexacontane		0.14	0.11	88	7667-80-3
112	27.562	27.525	27.615	TIC	4382377	1787405	2.45	0.09	   	Cholest-5-en-3-ol, (3.alpha.)-, TMS derivative		0.09	0.17	79	16134-40-0
113	27.650	27.615	27.670	TIC	1435547	521238	2.75	0.03	 V 	erythro-7,8-Bromochlorodisparlure		0.03	0.05	77	0-00-0
114	27.710	27.670	27.735	TIC	2628297	749620	3.51	0.06	 V 	erythro-7,8-Bromochlorodisparlure		0.06	0.07	75	0-00-0
115	27.769	27.735	27.825	TIC	8302360	2405078	3.45	0.17	 V 	Per-O-trimethylsilyl-(3-O-.beta.-d-mannopyranosyl-d-glucitol)		0.17	0.23	56	0-00-0
116	27.886	27.825	27.920	TIC	7790899	2112551	3.69	0.16	 V 	erythro-7,8-Bromochlorodisparlure		0.16	0.20	63	0-00-0
117	27.960	27.920	28.015	TIC	9604099	2442946	3.93	0.20	 V 	Tetrapentacontane, 1,54-dibromo-		0.20	0.24	69	0-00-0
118	28.082	28.015	28.210	TIC	115691327	49606216	2.33	2.43	 V 	2-Phenyl-1,2-propanediol, 2TMS derivative		2.43	4.78	60	294847-15-7
119	28.300	28.210	28.355	TIC	4283140	723009	5.92	0.09	 V 	erythro-7,8-Bromochlorodisparlure		0.09	0.07	73	0-00-0
120	28.407	28.355	28.435	TIC	1984148	656901	3.02	0.04	 V 	erythro-7,8-Bromochlorodisparlure		0.04	0.06	75	0-00-0
121	28.445	28.435	28.480	TIC	554764	348417	1.59	0.01	 V 	erythro-7,8-Bromochlorodisparlure		0.01	0.03	70	0-00-0
122	28.660	28.645	28.690	TIC	401287	146400	2.74	0.01	   	[1,1'-Bicyclohexyl]-4-carboxylic acid, 4'-pentyl-, 4-fluorophenyl ester		0.01	0.01	64	88878-50-6
123	28.715	28.690	28.745	TIC	1325642	519101	2.55	0.03	 V 	erythro-7,8-Bromochlorodisparlure		0.03	0.05	67	0-00-0
124	28.787	28.745	28.810	TIC	3058156	998419	3.06	0.06	 V 	erythro-7,8-Bromochlorodisparlure		0.06	0.10	69	0-00-0
125	28.842	28.810	28.895	TIC	3474343	913638	3.80	0.07	 V 	erythro-7,8-Bromochlorodisparlure		0.07	0.09	70	0-00-0
126	28.945	28.895	28.960	TIC	2181913	674985	3.23	0.05	 V 	erythro-7,8-Bromochlorodisparlure		0.05	0.06	67	0-00-0
127	28.991	28.960	29.025	TIC	3772059	1246549	3.03	0.08	 V 	erythro-7,8-Bromochlorodisparlure		0.08	0.12	63	0-00-0
128	29.070	29.025	29.085	TIC	2075659	603948	3.44	0.04	 V 	erythro-7,8-Bromochlorodisparlure		0.04	0.06	64	0-00-0
129	29.120	29.085	29.180	TIC	1701485	495529	3.43	0.04	 V 	[1,1'-Bicyclohexyl]-4-carboxylic acid, 4'-pentyl-, 4-fluorophenyl ester		0.04	0.05	63	88878-50-6
130	29.255	29.180	29.305	TIC	1235652	316030	3.91	0.03	   	Cyclohexane, 1,2,3,5-tetraisopropyl-		0.03	0.03	61	0-00-0
131	29.438	29.390	29.535	TIC	23708910	9995806	2.37	0.50	   	Phenol, 2,4-bis(1,1-dimethylethyl)-, phosphite (3:1)		0.50	0.96	82	31570-04-4
132	29.567	29.540	29.605	TIC	694911	367067	1.89	0.01	   	D-Homo-24-nor-17-oxachola-20,22-diene-3,16-dione, 1,2:14,15:21,23-triepoxy-7-hydroxy-4,4,8-trimethyl-, (5.alpha.,7.alpha.,13.al		0.01	0.04	56	35963-08-7
133	29.784	29.745	29.815	TIC	378371	159594	2.37	0.01	   	Cyclotrisiloxane, 2,4,6-trimethyl-2,4,6-triphenyl-		0.01	0.02	58	546-45-2
134	29.916	29.815	29.960	TIC	2808017	712816	3.94	0.06	 V 	Succinic acid, dodec-2-en-1-yl 2,2,3,3,4,4,5,5-octafluoropentyl ester		0.06	0.07	52	0-00-0
135	30.005	29.960	30.015	TIC	464393	99316	4.68	0.01	 V 	Cyclotrisiloxane, 2,4,6-trimethyl-2,4,6-triphenyl-		0.01	0.01	61	546-45-2
136	30.123	30.055	30.165	TIC	1453003	375303	3.87	0.03	   	Cyclotrisiloxane, 2,4,6-trimethyl-2,4,6-triphenyl-		0.03	0.04	60	546-45-2
137	30.180	30.165	30.195	TIC	127054	102946	1.23	0.00	 V 	Cyclotrisiloxane, 2,4,6-trimethyl-2,4,6-triphenyl-		0.00	0.01	63	546-45-2
138	30.967	30.885	31.290	TIC	127956860	26457584	4.84	2.69	S  	Propanoic acid, 3,3'-thiobis-, didodecyl ester		2.69	2.55	76	123-28-4
139	31.263	31.235	31.280	TIC	330463	187263	1.76	0.01	T  	Cyclotrisiloxane, 2,4,6-trimethyl-2,4,6-triphenyl-		0.01	0.02	57	546-45-2
140	31.342	31.290	31.495	TIC	14457145	3371260	4.29	0.30	SV 	Tris(2,4-di-tert-butylphenyl) phosphate		0.30	0.32	53	95906-11-9
141	31.450	31.435	31.470	TIC	36292	51203	0.71	0.00	T  	Cyclotrisiloxane, 2,4,6-trimethyl-2,4,6-triphenyl-		0.00	0.00	66	546-45-2

