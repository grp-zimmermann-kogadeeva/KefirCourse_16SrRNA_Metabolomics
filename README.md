# EMBO Practical Course 2024 [Metabolite and species dynamics in microbial communities](https://www.embl.org/about/info/course-and-conference-office/events/mcd22-01/) <br /> Data analysis - Day 4 💻

Welcome! This repository contains the materials for the computational data analysis sessions on Day 4. We will map our 16S reads to species, and visualise the dynamics of our kefir community and its metabolites. 



## 🏁 1. Setting up your VM to use the materials
The following steps describe how you get an up-to-date copy of this repo onto your VM and and define a variable to locate the path to your personal copy.

### Step 1: open a terminal on your VM
To open a terminal, use the keyboard shortcut **CTRL-Alt-T**. You can also open your home folder in the file browser via the bookmark on your desktop, right click below the folders and select "Open in terminal". Both alternatives open the terminal such that your current path in the terminal is your home folder.

**All commands shown below must be copied and pasted in this terminal window.** To **copy**, either highlight and then copy with CTRL+F or use the "Copy to clipboard" button that might (does not always) show up on the right when you hover over the code you want to copy. To **paste** in the terminal right click and select "paste" in the terminal window.

### Step 2: get your own copy of the repository to work in  
For you to get the newest version of the repo we will clone it, copy it to your home folder that is. 
```bash
git clone https://git.embl.de/grp-zimmermann-kogadeeva/KefirCourse_16SrRNA_Metabolomics.git 
```
Now you will find a folder `KefirCourse_16SrRNA_Metabolomics` in your home folder.

### Step 3: Define a variable containing the root path of your local repo copy

The variable `HOME` is by default containing the path of your home folder on a LINUX system. Therefore, we don't have to create this variable before using it. Move into your repo folder using `cd` and Set the variable `ROOT` as the root of your `16SrRNA_Metabolomics` repo like this:
```bash
cd ${HOME}/KefirCourse_16SrRNA_Metabolomics 
ROOT=$(pwd)
```

Now, you are able to always quickly navigate to your repository root folder from any directory, in your terminal like that:
```bash
cd ${ROOT}
```


## 🧬 2. 16S Nanopore sequencing data analysis

- Analysis of the Kefir Nanopore sequence data with NanoClust
- Plot time course of relative community composition
- Assess statistical significance of changes in community composition

Continue [here](https://git.embl.de/grp-zimmermann-kogadeeva/KefirCourse_16SrRNA_Metabolomics/-/blob/main/scripts/16S/NanoCLUST_16S_analysis.md).



## 🍬 3. Metabolomics data analysis

- Plot time course of metabolite abundances

Continue [here](https://git.embl.de/grp-zimmermann-kogadeeva/KefirCourse_16SrRNA_Metabolomics/-/blob/main/scripts/metabolomics/Visualization_of_metabolomics_results_MRM.md)
