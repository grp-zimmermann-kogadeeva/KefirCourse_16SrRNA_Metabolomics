Visualization_of_metabolomics_results_MRM
================
Eva Geissen, Maria Z.
2024-10-08

To open this notebook run

``` bash
conda activate metabolomics && rstudio
```

in the terminal

This R-markdown document loads the processed metabolomics data, adds
metadata, plots the metabolite abundances over time and assess the
statistical significance of the fold change. The code in this notebook
and the auxiliary functions file was modified from code provided by
Eleonora Mastrorilli.

Open the file `Visualization_of_metabolomics_results_2024.Rmd` in
Rstudio to interactively run through each code chunk. To run a code
chunk (grey boxes) click on the small green triangle in the upper right
corner of the box or use the keyboard short cut Ctrl+Alt+C. For more
details see the [Rmd
documentation](https://rmarkdown.rstudio.com/articles_intro.html).

``` r
# you have to uncomment and run this code if these packages are not installed on your platform
# install.packages("xlsx") 
# install.packages("coin") 
# install.packages("stringr") 
# install.packages("data.table") 
# install.packages("tidyverse") 
# install.packages("reader") 
# install.packages("ggplot2") 
# install.packages("survival") 
```

### Packages

Load the required R packages

``` r
library(xlsx)
library(coin)
library(stringr)
library(data.table)
library(tidyverse)
library(reader)
library(ggplot2)
library(survival)
library(vegan)
```

### Specify paths and source auxiliary functions

Set path variables for your working directory and the location of the
data and metadata. The path given is the path to the data produced in
the test run that are contained in the repository. We will provide you
with the path to your own data to replace

**You need to replace `<your_user>` in the code cell below with your
user name.**

``` r
# set path to repository root. You need to replace <your_user> with your user name

data_root = "/g/teachinglab/data/EMBO_MCD2024/Metabolomics"

raw_data_path <- file.path(data_root,"Results_MRM.csv")
metadata_path <- file.path(data_root,"Metadata.csv")

code_root = "~/KefirCourse_16SrRNA_Metabolomics"

source(file.path(code_root, "scripts/metabolomics/auxiliary_functions.R"), local = knitr::knit_global())
```

### Data wrangling

Before we can plot the data we need to wrangle it a bit to get it the
shape we want it. We will add some metadata, select the subset of
metabolites to plot, plot metabolite abundance over time and assess the
statistical significance of the fold change.

``` r
# read the input data file
data_df <- read.csv(raw_data_path)
# load meta data
metadata_df <- read.csv(metadata_path)
```

Now let’s pivot the table to the long format to have a single column
with metabolite intensities

``` r
# read the input data file
data_df_pivoted <- data_df %>%
  pivot_longer(
    cols = -Metabolite,
    names_to = "SampleName",
    values_to = "Area"
    )
```

Add metadata info to the pivoted dataframe

``` r
# read the input data file
data_metadata_df <- data_df_pivoted %>%
  left_join(metadata_df, by="SampleName")
```

Next we convert time point and replicate to numeric values

``` r
# convert time point to numeric
data_metadata_df <- data_metadata_df %>%
     mutate(Time = as.numeric(gsub("[^0-9]","",TimePoint)))
#replace milk that turned into NA by -1
data_metadata_df <- data_metadata_df %>%
  mutate(Time = replace(Time, is.na(Time), "-1"))
# set column type as numeric
data_metadata_df <- data_metadata_df %>%
  mutate(Time = as.numeric(Time))

#convert replicate number to numeric
data_metadata_df <- data_metadata_df %>%
     mutate(ReplicateNum = as.numeric(gsub("[^0-9]","",Replicate)))
```

Finally, convert to data.table for subsequent plotting

``` r
# convert from data.frame to  data.table
my_df <- data_metadata_df
setDT(my_df)
```

Now the data is in `my_df` and ready for plotting.

### Plotting of AUC time courses

First we will plot the Compound time course for each compound
separately. We join the milk and the kefir samples by replicate (column
ReplicateNum).

``` r
# prepare data.table
my_prep_df = my_df
# setkey sorts a data.table and marks it as sorted with an attribute "sorted". To learn about advatages of setting key, type ?setkey
setkey(my_prep_df,Metabolite,Type,Replicate,Time)
  

# one plot for each compound  
for (d in levels(as.factor(my_prep_df$Metabolite))){
  # extract the subset of data we need
  df <- my_prep_df[.(d), .(Area, Time, Replicate, Type)]
  df  
  if (all(is.na(df[,.(Area)]))){
    cat(paste(d," has no AUC intensity data. Skipping plot. \n",sep=""))
    next
  }else{
    # create the plot
     p= ggplot(df, aes(x = Time, y = Area, group = interaction(Replicate, Type), colour = Replicate)) +
          geom_line(aes(group = interaction(Replicate, Type)))+
          geom_point(aes(group = interaction(Replicate, Type)))+
          scale_y_log10()+
          scale_x_continuous(breaks = unique(df$Time))+
          ggtitle(paste("AUC for ", d, " over Time", sep=""),    # add title
                  subtitle = paste("Separated by Replicate and Type"))
    # display the plot
    print(p)  
  }
} 
```

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-10-1.png)<!-- -->![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-10-2.png)<!-- -->![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-10-3.png)<!-- -->![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-10-4.png)<!-- -->![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-10-5.png)<!-- -->![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-10-6.png)<!-- -->![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-10-7.png)<!-- -->![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-10-8.png)<!-- -->![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-10-9.png)<!-- -->![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-10-10.png)<!-- -->![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-10-11.png)<!-- -->![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-10-12.png)<!-- -->![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-10-13.png)<!-- -->![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-10-14.png)<!-- -->![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-10-15.png)<!-- -->![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-10-16.png)<!-- -->![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-10-17.png)<!-- -->![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-10-18.png)<!-- -->![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-10-19.png)<!-- -->

We create another plot to plot the mean and standard error of all
metabolites over time together in one plot.

``` r
# all together
my_prep_df <- my_df
my_prep_df <- my_prep_df %>% mutate_cond(Type == 'Milk', Type="Kefir")


my_prep_df = as.data.table(my_prep_df)
setkey(my_prep_df,Metabolite, Type, Replicate, Time)
  

# account for missing data     
if (all(is.na(my_prep_df[,.(Area)]))){
  cat(paste(d," has no AUC intensity data. Skipping plot. \n",sep=""))
  next
}else{
      p <- ggplot(my_prep_df, aes(x = Time, y = Area, group = interaction(Metabolite, Type), colour = Metabolite)) +
          geom_point(aes(group = interaction(Metabolite, Type)))+
          stat_summary(fun.data ='mean_se', geom = "smooth", se = TRUE, alpha=0.1)  + #, colour='grey')+
          # scale_y_log10()+
          scale_x_continuous(breaks = unique(my_prep_df$Time))+
          ggtitle(paste("AUC for all compounds over Time", sep=""))+
          labs(x = "Time [h]",
               y = "Area [au]",
               color = "Compound")
      print(p)
}
```

    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-11-1.png)<!-- -->
\### Plotting of scaled time courses

In addition, you can plot the time courses of each compound relative to
the first time point the the compound was detected. To do so, we first
do this scaling

``` r
my_final_df <- my_prep_df
setkey(my_final_df, Metabolite, Replicate, Type, Time)

# add column Scaled_Area
my_final_df[, Scaled_Area := scale_to_t0(.SD),
            by = .(Replicate, Type, Metabolite)]
```

Then we do the plotting. First by compound and replicate separately.

``` r
for (d in levels(droplevels(as.factor(my_final_df$Metabolite)))){
  # extract the subset of data we need
  df <- my_final_df[Metabolite==d, .(Scaled_Area, Time, Replicate, Type)]
  df$Sample = as.character(df$Type)
    
  if (all(is.na(df[,.(Scaled_Area)]))){
    cat(paste(d," has no scaled AUC intensity data. Skipping plot. \n",sep=""))
    next
  }else{

    p <-  ggplot(df, aes(x=Time,y= Scaled_Area, color = Replicate , group = interaction(Replicate, Type))) +
        geom_point() +
        stat_summary(fun.data ='mean_se', geom = "smooth", se = TRUE, alpha=0.1)  + #, colour='grey')+
        scale_x_continuous(breaks = unique(df$Time))+
        coord_cartesian(ylim = c(0, max(df$Scaled_Area))) +
        #theme_bw() +
        ggtitle(paste("Scaled AUC for ", d, " over Time", sep=""),
                subtitle = paste("Separated by Pool and Sample")) + #theme(legend.position = "none")  +
        labs(x = "Time [h]",
             y = "Scaled Area [au]",
             color = "Legend") 
    
    print(p)
  }
}
```

    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-13-1.png)<!-- -->

    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-13-2.png)<!-- -->

    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-13-3.png)<!-- -->

    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-13-4.png)<!-- -->

    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-13-5.png)<!-- -->

    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-13-6.png)<!-- -->

    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-13-7.png)<!-- -->

    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-13-8.png)<!-- -->

    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-13-9.png)<!-- -->

    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-13-10.png)<!-- -->

    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-13-11.png)<!-- -->

    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-13-12.png)<!-- -->

    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-13-13.png)<!-- -->

    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-13-14.png)<!-- -->

    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-13-15.png)<!-- -->

    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-13-16.png)<!-- -->

    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-13-17.png)<!-- -->

    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-13-18.png)<!-- -->

    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-13-19.png)<!-- -->

In addition, you can plot the mean and the standard error over all
scaled replicates of all compounds in a single plot

``` r
setkey(my_final_df,Metabolite,Type, Replicate, Time)
  
if (all(is.na(my_final_df[,.(Scaled_Area)]))){
  cat(paste(d," has no AUC intensity data. Skipping plot. \n",sep=""))
  next
}else{

  p <- ggplot(my_prep_df, aes(x = Time, y = Scaled_Area, group = interaction(Metabolite, Type), colour = Metabolite)) +
        # geom_point(aes(group = interaction(Compound, Sample)))+
        stat_summary(fun.data ='mean_se', geom = "smooth", se = TRUE, alpha=0.1)  + #, colour='grey')+
        scale_x_continuous(breaks = unique(my_prep_df$Time))+
        #scale_y_continuous(trans = "log2")+    # set y axis to log2 scale
        ggtitle(paste("Scaled Area for all compounds over Time", sep=""))+
        labs(x = "Time [h]",
             y = "Scaled Area [au]",
             color = "Compound")
      
  print(p)
      
    }
```

    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?
    ## `geom_line()`: Each group consists of only one observation.
    ## ℹ Do you need to adjust the group aesthetic?

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-14-1.png)<!-- -->

### Statistical assessment of significance of metabolite fold changes

Now we will assess the statistical significance of the fold change
between the first and the last time point a metabolite was detected.

First we compute the mean AUC for each compound at each time point and
add it as an additional column:

``` r
# for each Pool, Sample, Compound, compute FC with respect to t0 (i.e. divide by the intensity at t0)
my_stat_df <- my_prep_df
# calculate mean of technical replicates (e,g, A1 and B1, A2 and B2, A3 and C3) before calculating statistics between time points
#add column by which to group replicates
my_stat_df <- my_stat_df %>%
  mutate(Tech_replicate_names = paste0(Metabolite, "_", TimePoint, "_", ReplicateNum))
test_df <- my_stat_df %>%
  group_by(Tech_replicate_names) %>%
  summarize(Area = mean(Area))
# add all other columns from my_stat_df
my_stat_df <- my_stat_df %>%
  filter(grepl("A", Replicate)) %>%
  select(-Area)
my_stat_df  <- my_stat_df  %>%
  left_join(test_df, by="Tech_replicate_names")

my_stat_df[, Mean_Area_per_timepoint := mean(Area, na.rm = T), by = .(Metabolite, Type, Time)]
```

    ## Warning in `[.data.table`(my_stat_df, , `:=`(Mean_Area_per_timepoint,
    ## mean(Area, : Invalid .internal.selfref detected and fixed by taking a (shallow)
    ## copy of the data.table so that := can add this new column by reference. At an
    ## earlier point, this data.table has been copied by R (or was created manually
    ## using structure() or similar). Avoid names<- and attr<- which in R currently
    ## (and oddly) may copy the whole data.table. Use set* syntax instead to avoid
    ## copying: ?set, ?setnames and ?setattr. If this message doesn't help, please
    ## report your use case to the data.table issue tracker so the root cause can be
    ## fixed or this message improved.

``` r
setkey(my_stat_df, Type, Metabolite, Replicate, Time)
```

Now we compute the fold change between the mean values for each
metabolite at each time point with respect to the first time point it
was detected and check the significance of the change based on the data
for the first and last time point a metabolite was detected.

For this we first add a column that contains the time ranking of data
points of a metabolite. We need to do this because not all compounds
where detectable at the actually first and actual last sample time
point. Our strategy is to compute the fold change for reach time rank
separately, use the data for the minimal and maximal rank to compute a
p-value and then put everything back together.

``` r
# add column with mean 
my_stat_df[, ranked_time := as.numeric(factor(rank(Time))), by=.(Metabolite, Type)]
##### split - do - rbind
List_subtables = list()

# let's split tables iteratively keeping only the rank we're interested in
# and  rank 1 cause we need it to calculate the fold change
for(i in unique(my_stat_df$ranked_time)){
  my_name = paste("temp_df", i, sep  = "_")
  temp <- my_stat_df[ranked_time %in% c(1, i)]

  cat(paste("Analyzing ranked time ",i,'...\n',sep=''))
  temp[, FC := compute_FC(.SD), by = .(Type, Metabolite)]
  temp[, FC_pval := FC_significance(.SD), by = .(Type, Metabolite)]

  # remove time0 from all other timepoints
  if(i!=1){
    temp <- temp[ranked_time != 1 ,]
  }

  #  give the variable 'temp' a variable name that depends on the loop index (my_name)
  assign(my_name, temp)
  List_subtables[[my_name]] = temp
}
```

    ## Analyzing ranked time 1...
    ## Analyzing ranked time 2...
    ## Analyzing ranked time 3...
    ## Analyzing ranked time 4...
    ## Analyzing ranked time 5...

``` r
# check if the dimensions  to see that that we did not loose data points
stopifnot(sum(unlist(lapply(List_subtables, nrow)))==nrow(my_stat_df))

my_stat_df_backup <- my_stat_df

# put the individual lists back together
my_stat_df <- rbindlist(List_subtables)

setkey(my_stat_df, Type, Metabolite, Replicate, Time)
```

To make the display of the outcome more compact, we now reduce the data
to the interesting columns. In addition we correct the p-values for
multiple comparisons and add the corrected p-values as a column.

``` r
my_red_df <- unique(my_stat_df[,.(Metabolite, Type, Time,
                                   Mean_Area_per_timepoint, FC, FC_pval)])

# correct pvalues
my_red_df[!is.na(my_red_df$FC_pval),
          FC_pval_corrected := p.adjust(FC_pval, method= "fdr"),
          by = .(Type, Metabolite)]
```

Now we can have a look at our results and save them to an excel file.

``` r
View(my_red_df)
output_path <- '/home/training18/metabolite_fold_change_stats.csv'
write.csv2(my_red_df, output_path)
```

Finally, we add all our computed information to our full data frame. To
keep track of them

``` r
my_stat_df <- my_red_df[my_stat_df, on = c("Metabolite", "Type", "Time",
                                             "Mean_Area_per_timepoint", "FC", "FC_pval")]
```

# Ordination analysis of metabolomics samples

\#create count matrix for each compound individually and store them in a
list

``` r
# make a short name for the samples
my_final_df <- my_final_df  %>%
  mutate(ShortName = paste0(TimePoint, '_', Replicate))

compound_Area_list <- list()

c.mat <- 
  my_final_df %>% 
  select(ShortName, Metabolite, Scaled_Area) %>% 
  pivot_wider(names_from = ShortName,values_from = Scaled_Area, values_fill = 0) %>% 
  as.data.frame() %>% 
  column_to_rownames("Metabolite") %>% 
  as.matrix()

compound_Area_list <- c.mat
```

\#Ordination analysis of metabolomics samples

``` r
# Get pairwise manhattan distances of samples over compounds
pairwise_d <- vegan::vegdist(t(compound_Area_list), method = 'manhattan')

# Get ordination plot (PCoA)
#pcoa <- cmdscale(t(pairwise_d), k = 2)
pcoa_results <- cmdscale(t(pairwise_d), k = 2, eig = TRUE);
pcoa <- pcoa_results$points
#PC,Score,latent] = princomp(X);

colnames(pcoa) <- c("PCo 1", "PCo 2")
pcoa <- pcoa %>%
  as.data.frame() %>%
  mutate(sampleID = rownames(.)) %>%
  relocate(sampleID) %>%
  as_tibble() %>%
  mutate(sampleID = factor(sampleID, levels = sampleID))

set.seed(2)
options(repr.plot.width=3.5, repr.plot.height=3.5)
ordinationplot <- pcoa %>%
  ggplot(aes(x = `PCo 1`, y = `PCo 2`)) +
  geom_point() +
  geom_text(aes(label = sampleID), nudge_x = runif(n = dim(pcoa)[1],
                                                   min = -2,
                                                   max = 2), nudge_y = runif(n = dim(pcoa)[1], min = 0, max = 1)) +
  theme_bw()
ordinationplot
```

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-21-1.png)<!-- -->

``` r
# Plot eigenvalues of PCCoA analysis 
pccoa_eig <- pcoa_results$eig

barplot(pccoa_eig,
        main = "Eigenvalues of PCCoA analysis",
        xlab = "Eigenvalue number",
        ylab = "Eigenvalue")
```

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-22-1.png)<!-- -->

\#Compare principal coordinate analysis with principal component
analysis.

``` r
# Perform PCA (principal components analysis)
species.pca <- rda(t(compound_Area_list))
```

Plot principal components and loadings plot. More on PCA components and
loadings you can read, for example, here:
<https://towardsdatascience.com/what-are-pca-loadings-and-biplots-9a7897f2e559>

``` r
# Perform PCA (principal components analysis)
biplot(species.pca)
```

![](Visualization_of_metabolomics_results_MRM_files/figure-gfm/unnamed-chunk-24-1.png)<!-- -->

Display summary of PCA analysis.

``` r
# Summary pf PCA results
summary(species.pca)
```

    ## 
    ## Call:
    ## rda(X = t(compound_Area_list)) 
    ## 
    ## Partitioning of variance:
    ##               Inertia Proportion
    ## Total            3807          1
    ## Unconstrained    3807          1
    ## 
    ## Eigenvalues, and their contribution to the variance 
    ## 
    ## Importance of components:
    ##                             PC1      PC2      PC3       PC4      PC5       PC6
    ## Eigenvalue            3577.2067 178.9221 29.65496 15.220715 3.858782 1.2817601
    ## Proportion Explained     0.9397   0.0470  0.00779  0.003998 0.001014 0.0003367
    ## Cumulative Proportion    0.9397   0.9867  0.99450  0.998497 0.999511 0.9998476
    ##                             PC7       PC8       PC9      PC10      PC11
    ## Eigenvalue            3.798e-01 1.038e-01 5.209e-02 1.947e-02 8.773e-03
    ## Proportion Explained  9.978e-05 2.727e-05 1.368e-05 5.113e-06 2.305e-06
    ## Cumulative Proportion 9.999e-01 1.000e+00 1.000e+00 1.000e+00 1.000e+00
    ##                            PC12      PC13      PC14      PC15      PC16
    ## Eigenvalue            7.746e-03 3.476e-03 2.275e-03 1.563e-03 7.435e-04
    ## Proportion Explained  2.035e-06 9.132e-07 5.975e-07 4.107e-07 1.953e-07
    ## Cumulative Proportion 1.000e+00 1.000e+00 1.000e+00 1.000e+00 1.000e+00
    ##                            PC17      PC18      PC19
    ## Eigenvalue            2.019e-04 9.676e-05 7.245e-05
    ## Proportion Explained  5.303e-08 2.542e-08 1.903e-08
    ## Cumulative Proportion 1.000e+00 1.000e+00 1.000e+00

*Questions:* - What is the difference between principal component and
principal coordinate analysis? - What are the metabolites that
contribute most to PC1 and PC2 projections? - Why are some samples
located separately from others on the PCCoA and PCA plots?
