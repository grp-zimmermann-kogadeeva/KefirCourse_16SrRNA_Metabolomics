Visualization of metabolomimcs results
================
Eva Geissen
12/10/2022

To open this notebook run

``` bash
conda activate && rstudio
```

in the terminal

This R-markdown document loads the processed metabolomics data, adds
metadata, plots the metabolite abundances over time and assess the
statistical significance of the fold change. The code in this notebook
and the auxiliary functions file was modified from code provided by
Eleonora Mastrorilli.

Open the file `Visualization_of_metabolomics_results.Rmd` in Rstudio to
interactively run through each code chunk. To run a code chunk (grey
boxes) click on the small green triangle in the upper right corner of
the box or use the keyboard short cut Ctrl+Alt+C. For more details see
the [Rmd
documentation](https://rmarkdown.rstudio.com/articles_intro.html).

``` r
# install.packages("xlsx") 
# install.packages("coin") 
# install.packages("stringr") 
# install.packages("data.table") 
# install.packages("tidyverse") 
# install.packages("reader") 
# install.packages("ggplot2") 
# install.packages("survival") 
```

### Packages

Load the required R packages

``` r
library(xlsx)
library(coin)
library(stringr)
library(data.table)
library(tidyverse)
library(reader)
library(ggplot2)
library(survival)
library(vegan)
```

### Specify paths and source auxiliary functions

Set path variables for your working directory and the location of the
data and metadata. The path given is the path to the data produced in
the test run that are contained in the repository. We will provide you
with the path to your own data to replace

**You need to replace `<your_user>` in the code cell below with your
user name.**

``` r
# set path to repository root. You need to replace <your_user> with your user name

#my_root = "/home/<your_user>/embo_mcd22"
# my_root = "/home/training15/embo_mcd22"
my_root ="C:/Users/geissen/ownCloud/Documents/CBM/teaching/MCD22/embo_mcd22"



raw_data_path <- file.path(my_root,"data/metabolomics/Raw_data")
metadata_path <- file.path(my_root,"data/metabolomics/Metadata")


source(file.path(my_root, "scripts/metabolomics/auxiliary_functions.R"), local = knitr::knit_global())
```

Have a look at the content of the data folder.

``` r
list.files(raw_data_path)
```

    ##  [1] "ASCIIData005.txt" "ASCIIData006.txt" "ASCIIData007.txt" "ASCIIData009.txt"
    ##  [5] "ASCIIData010.txt" "ASCIIData011.txt" "ASCIIData012.txt" "ASCIIData013.txt"
    ##  [9] "ASCIIData014.txt" "ASCIIData015.txt" "ASCIIData016.txt" "ASCIIData017.txt"
    ## [13] "ASCIIData018.txt" "ASCIIData019.txt" "ASCIIData020.txt" "ASCIIData021.txt"
    ## [17] "ASCIIData022.txt" "ASCIIData023.txt" "ASCIIData024.txt" "ASCIIData025.txt"
    ## [21] "ASCIIData026.txt" "ASCIIData027.txt" "ASCIIData028.txt" "ASCIIData029.txt"
    ## [25] "ASCIIData030.txt" "ASCIIData031.txt" "ASCIIData032.txt"

### Data wrangling

Before we can plot the data we need to wrangle it a bit to get it the
shape we want it. We will add some metadata, select the subset of
metabolites to plot, plot metabolite abundance over time and assess the
statistical significance of the fold change.

``` r
# read each data file and collate
data_df <- data.frame()
for(i in list.files(raw_data_path)){
  data_df <- rbind.data.frame(data_df,
                              read_shimadzu_gcms(raw_data_path,i))
}
```

We now filter only for the metabolites we are interested in. A list of
the interesting metabolites is in the Metadata path. We load this and
use it to filter.

``` r
# load meta data
metadata_df <- read.xlsx(file.path(metadata_path,'Targeted_molecules.xlsx'),sheetIndex = 1)
unique(metadata_df$Name)
```

    ## [1] Lactic acid, 2TMS derivative                         
    ## [2] Glycerol, 3TMS derivative                            
    ## [3] Butanedioic acid, 2TMS derivative                    
    ## [4] L-Proline, 2TMS derivative                           
    ## [5] Citric acid, 4TMS derivative                         
    ## [6] Ethyl .alpha.-D-glucopyranoside, 4TMS derivative     
    ## [7] D-Lactose, octakis(trimethylsilyl) ether, methyloxime
    ## [8] Maltose                                              
    ## 8 Levels: Butanedioic acid, 2TMS derivative ... Maltose

``` r
filtered_df <- data.frame()

# filter
for(f in unique(metadata_df$Name)){
  filtered_df <- rbind.data.frame(filtered_df,
                                  filter_feature(data_df, 
                                                 feature_name = f, 
                                                 proc_from = metadata_df$Proc.From[metadata_df$Name==f], 
                                                 proc_to = metadata_df$Proc.To[metadata_df$Name==f]))
}
```

Lactic acid is sometimes annotated as “D-(-)-Lactic acid, 2TMS
derivative” and sometimes as “L-(-)-Lactic acid, 2TMS derivative”. We
will replace both with “Lactic acid, 2TMS derivative” to make it easier.

``` r
filtered_df <- filtered_df %>% mutate(Name = ifelse(Name=="D-(-)-Lactic acid, 2TMS derivative", "Lactic acid, 2TMS derivative", Name),
                                      Name = ifelse(Name=="L-(+)-Lactic acid, 2TMS derivative", "Lactic acid, 2TMS derivative", Name),
                                      Name = ifelse(Name=="Maltose, octakis(trimethylsilyl) ether, methyloxime (isomer 1)", "D-Lactose", Name))
```

Replace column names to something more descriptive.

``` r
colnames(filtered_df)[colnames(filtered_df)=="Name"] <- 'Compound'
colnames(filtered_df)[colnames(filtered_df)=="Ret.Time"] <- 'RT'
```

Next, we retrieve the information about sample identity (milk or kefir),
time point and replicate number (Pool), contained in column `file` to
make additional columns with this information

``` r
# obtain useful info from Sample name
filtered_df <- filtered_df %>% 
  separate("file", c("Sample", "Time", "Pool"),remove = F)
# adjust for Milk
filtered_df <- filtered_df %>% 
  mutate_cond(Sample=="Milk", Pool = Time, Time = "T-1")
```

Finally, we remove unused chars in columns `Pool` and `Time` and convert
them to numeric.

``` r
filtered_df <- filtered_df %>%
  mutate(Time = sapply(Time, 
                       function(x) gsub("T","",x,fixed = T)),
         Pool = sapply(Pool,
                       function(x) str_sub(x,-1,-1)))
filtered_df <- filtered_df %>%
  mutate(Time = as.numeric(Time),
         Pool = as.factor(Pool))

# convert from data.frame to  data.table
my_df <- filtered_df
setDT(my_df)
```

Now the data is in `my_df` and ready for plotting.

### Plotting of AUC time courses

First we will plot the Compound time course for each compound
separately. We join the milk and the kefir samples by replicate (called
pool).

``` r
# prepare data.table
my_prep_df = my_df
setkey(my_prep_df,Compound,Sample,Pool,Time)
  

# one plot for each compound  
for (d in levels(as.factor(my_prep_df$Compound))){
  # extract the subset of data we need
  df <- my_prep_df[.(d), .(Area, Time, Pool, Sample)]
  df  
  if (all(is.na(df[,.(Area)]))){
    cat(paste(d," has no AUC intensity data. Skipping plot. \n",sep=""))
    next
  }else{
       #facet.factors <- c("Sample")
        
     p= ggplot(df, aes(x = Time, y = Area, group = interaction(Pool, Sample), colour = Pool)) +
          geom_line(aes(group = interaction(Pool, Sample)))+
          geom_point(aes(group = interaction(Pool, Sample)))+
          # facet_wrap(facet.factors, nrow = 1) +  # if split in facets by "Sample" is wanted
          # geom_hline(yintercept=5000, color='red', linetype = "dashed") +  # add the noise level threshold
          # facet_grid(. ~ df$Plate + df$Sample , margins = TRUE) +
          scale_y_log10()+
          scale_x_continuous(breaks = unique(df$Time))+
          ggtitle(paste("AUC for ", d, " over Time", sep=""),    # add title
                  subtitle = paste("Divided by Pool and Sample"))
       
    print(p)  

  }
} 
```

![](Visualization_of_metabolomics_results_files/figure-gfm/unnamed-chunk-12-1.png)<!-- -->![](Visualization_of_metabolomics_results_files/figure-gfm/unnamed-chunk-12-2.png)<!-- -->![](Visualization_of_metabolomics_results_files/figure-gfm/unnamed-chunk-12-3.png)<!-- -->![](Visualization_of_metabolomics_results_files/figure-gfm/unnamed-chunk-12-4.png)<!-- -->![](Visualization_of_metabolomics_results_files/figure-gfm/unnamed-chunk-12-5.png)<!-- -->![](Visualization_of_metabolomics_results_files/figure-gfm/unnamed-chunk-12-6.png)<!-- -->![](Visualization_of_metabolomics_results_files/figure-gfm/unnamed-chunk-12-7.png)<!-- -->

We create another plot to plot the mean and standard error of all
metabolites over time together in one plot.

``` r
# all together
my_prep_df <- my_df
my_prep_df <- my_prep_df %>% mutate_cond(Sample == 'Milk', Sample="Kefir")


my_prep_df = as.data.table(my_prep_df)
setkey(my_prep_df,Compound,Sample,Pool, Time)
  

# account for missing data     
if (all(is.na(my_prep_df[,.(Area)]))){
  cat(paste(d," has no AUC intensity data. Skipping plot. \n",sep=""))
  next
}else{
      # facet.factors <- c("Sample")
      p <- ggplot(my_prep_df, aes(x = Time, y = Area, group = interaction(Compound, Sample), colour = Compound)) +
          geom_point(aes(group = interaction(Compound, Sample)))+
          stat_summary(fun.data ='mean_se', geom = "smooth", se = TRUE, alpha=0.1)  + #, colour='grey')+
          # facet_wrap(facet.factors, nrow = 1) +
          # geom_hline(yintercept=5000, color='red', linetype = "dashed") +  # add the noise level threshold
          # facet_grid(. ~ df$Plate + df$Sample , margins = TRUE) +
          # scale_y_log10()+
          scale_x_continuous(breaks = unique(my_prep_df$Time))+
          ggtitle(paste("AUC for all compounds over Time", sep=""))+
          labs(x = "Time [h]",
               y = "Area [au]",
               color = "Compound")
      print(p)
}
```

![](Visualization_of_metabolomics_results_files/figure-gfm/unnamed-chunk-13-1.png)<!-- -->
\#\#\# Plotting of scaled time courses

In addition, you can plot the time courses of each compound relative to
the first time point the the compound was detected. To do so, we first
do this scaling

``` r
my_final_df <- my_prep_df
setkey(my_final_df, Compound, Pool, Sample, Time)

# add column Scaled_Area
my_final_df[, Scaled_Area := scale_to_t0(.SD),
            by = .(Pool, Sample, Compound)]
```

Then we do the plotting. First by compound and replicate separatly.

``` r
for (d in levels(droplevels(as.factor(my_final_df$Compound)))){
  # extract the subset of data we need
  df <- my_final_df[Compound==d, .(Scaled_Area, Time, Pool, Sample)]
  df$Sample = as.character(df$Sample)
    
  if (all(is.na(df[,.(Scaled_Area)]))){
    cat(paste(d," has no scaled AUC intensity data. Skipping plot. \n",sep=""))
    next
  }else{

    p <-  ggplot(df, aes(x=Time,y= Scaled_Area, color = Pool , group = interaction(Pool, Sample))) +
        geom_point() +
        stat_summary(fun.data ='mean_se', geom = "smooth", se = TRUE, alpha=0.1)  + #, colour='grey')+
        scale_x_continuous(breaks = unique(df$Time))+
        coord_cartesian(ylim = c(0, max(df$Scaled_Area))) +
        #theme_bw() +
        ggtitle(paste("Scaled AUC for ", d, " over Time", sep=""),
                subtitle = paste("Divided by Pool and Sample")) + #theme(legend.position = "none")  +
        labs(x = "Time [h]",
             y = "Scaled Area [au]",
             color = "Legend") 
    
    print(p)
  }
}
```

![](Visualization_of_metabolomics_results_files/figure-gfm/unnamed-chunk-15-1.png)<!-- -->![](Visualization_of_metabolomics_results_files/figure-gfm/unnamed-chunk-15-2.png)<!-- -->![](Visualization_of_metabolomics_results_files/figure-gfm/unnamed-chunk-15-3.png)<!-- -->![](Visualization_of_metabolomics_results_files/figure-gfm/unnamed-chunk-15-4.png)<!-- -->![](Visualization_of_metabolomics_results_files/figure-gfm/unnamed-chunk-15-5.png)<!-- -->

    ## geom_path: Each group consists of only one observation. Do you need to adjust
    ## the group aesthetic?

![](Visualization_of_metabolomics_results_files/figure-gfm/unnamed-chunk-15-6.png)<!-- -->![](Visualization_of_metabolomics_results_files/figure-gfm/unnamed-chunk-15-7.png)<!-- -->

In addition, you can plot the mean and the standard error over all
scaled replicates of all compounds in a single plot

``` r
setkey(my_final_df,Compound,Sample,Pool, Time)
  
if (all(is.na(my_final_df[,.(Scaled_Area)]))){
  cat(paste(d," has no AUC intensity data. Skipping plot. \n",sep=""))
  next
}else{

  p <- ggplot(my_prep_df, aes(x = Time, y = Scaled_Area, group = interaction(Compound, Sample), colour = Compound)) +
        # geom_point(aes(group = interaction(Compound, Sample)))+
        stat_summary(fun.data ='mean_se', geom = "smooth", se = TRUE, alpha=0.1)  + #, colour='grey')+
        # scale_y_log10()+    # set y axis to log10 scale
        scale_x_continuous(breaks = unique(my_prep_df$Time))+
        #scale_y_continuous(trans = "log2")+    # set y axis to log2 scale
        ggtitle(paste("Scaled Area for all compounds over Time", sep=""))+
        labs(x = "Time [h]",
             y = "Scaled Area [au]",
             color = "Compound")
      
  print(p)
      
    }
```

![](Visualization_of_metabolomics_results_files/figure-gfm/unnamed-chunk-16-1.png)<!-- -->

### Statistical assessment of significance of metabolite fold changes

Now we will assess the statistical significance of the fold change
between the first and the last time point a metabolite was detected.

First we compute the mean AUC for each compound at each time point and
add it as an additional column:

``` r
# for each Pool, Sample, Compound, compute FC with respect to t0 (i.e. divide by the intensity at t0)
my_stat_df <- my_prep_df
my_stat_df[, Mean_Area_per_timepoint := mean(Area, na.rm = T), by = .(Compound, Sample, Time)]
setkey(my_stat_df, Sample, Compound, Pool, Time)
```

Now we compute the fold change between the mean values for each
metabolite at each time point with respect to the first time point it
was detected and check the significance of the change based on the data
for the first and last time point a metabolite was detected.

For this we first add a column that contains the time ranking of data
points of a metabolite. We need to do this because not all compounds
where detectable at the actually first and actual last sample time
point. Our strategy is to compute the fold change for reach time rank
separately, use the data for the minimal and maximal rank to compute a
p-value and then put everything back together.

``` r
# add column with mean 
my_stat_df[, ranked_time := as.numeric(factor(rank(Time))), by=.(Compound,Sample)]
##### split - do - rbind
List_subtables = list()

# let's split tables iteratively keeping only the rank we're interested in
# and  rank 1 cause we need it to calculate the fold change
for(i in unique(my_stat_df$ranked_time)){
  my_name = paste("temp_df", i, sep  = "_")
  temp <- my_stat_df[ranked_time %in% c(1, i)]

  cat(paste("Analyzing ranked time ",i,'...\n',sep=''))
  temp[, FC := compute_FC(.SD), by = .(Sample,Compound)]
  temp[, FC_pval := FC_significance(.SD), by = .(Sample,Compound)]

  # remove time0 from all other timepoints
  if(i!=1){
    temp <- temp[ranked_time != 1 ,]
  }

  #  give the variable 'temp' a variable name that depends on the loop index (my_name)
  assign(my_name, temp)
  List_subtables[[my_name]] = temp
}
```

    ## Analyzing ranked time 1...
    ## Analyzing ranked time 2...
    ## Analyzing ranked time 3...
    ## Analyzing ranked time 4...
    ## Analyzing ranked time 5...
    ## Analyzing ranked time 6...
    ## Analyzing ranked time 7...

``` r
# check if the dimensions  to see that that we did not loose data points
stopifnot(sum(unlist(lapply(List_subtables, nrow)))==nrow(my_stat_df))

my_stat_df_backup <- my_stat_df

# put the individual lists back together
my_stat_df <- rbindlist(List_subtables)

setkey(my_stat_df, Sample, Compound, Pool, Time)
```

To make the display of the outcome more compact, we now reduce the data
to the interesting columns. In addition we correct the p-values for
multiple comparisons and add the corrected p-values as a column.

``` r
my_red_df <- unique(my_stat_df[,.(Compound, Sample, Time,
                                   Mean_Area_per_timepoint, FC, FC_pval)])

# correct pvalues
my_red_df[!is.na(my_red_df$FC_pval),
          FC_pval_corrected := p.adjust(FC_pval, method= "fdr"),
          by = .(Sample,Compound)]
```

Now we can have a look at our results and save them to an excel file.

``` r
View(my_red_df)
write.xlsx(my_red_df, file.path(my_root, "scripts/metabolomics/Fold_change_stats.xlsx"))
```

Finally, we add all our computed information to our full data frame. To
keep track of them

``` r
my_stat_df <- my_red_df[my_stat_df, on = c("Compound", "Sample", "Time",
                                             "Mean_Area_per_timepoint", "FC", "FC_pval")]
```

# Ordination analysis of metabolomics samples

\#create count matrix for each compound individually and store them in a
list

``` r
compound_Area_list <- list()
c.mat <- 
  my_final_df %>% 
  select(file,Compound,Area) %>% 
  pivot_wider(names_from = file,values_from = Area,values_fill = 0) %>% 
  as.data.frame() %>% 
  column_to_rownames("Compound") %>% 
  as.matrix()

compound_Area_list <- c.mat
```

\#Ordination analysis of metabolomics samples

``` r
# Get pairwise manhattan distances of samples over compounds
pairwise_d <- vegan::vegdist(t(compound_Area_list), method = 'manhattan')

# Get ordination plot (PCoA)
#pcoa <- cmdscale(t(pairwise_d), k = 2)
pcoa_results <- cmdscale(t(pairwise_d), k = 2, eig = TRUE);
pcoa <- pcoa_results$points
#PC,Score,latent] = princomp(X);

colnames(pcoa) <- c("PCo 1", "PCo 2")
pcoa <- pcoa %>%
  as.data.frame() %>%
  mutate(sampleID = rownames(.)) %>%
  relocate(sampleID) %>%
  as_tibble() %>%
  mutate(sampleID = factor(sampleID, levels = sampleID))

set.seed(2)
options(repr.plot.width=3.5, repr.plot.height=3.5)
ordinationplot <- pcoa %>%
  ggplot(aes(x = `PCo 1`, y = `PCo 2`)) +
  geom_point() +
  geom_text(aes(label = sampleID), nudge_x = runif(n = dim(pcoa)[1],
                                                   min = -2,
                                                   max = 2), nudge_y = runif(n = dim(pcoa)[1], min = 0, max = 1)) +
  theme_bw()
ordinationplot
```

![](Visualization_of_metabolomics_results_files/figure-gfm/unnamed-chunk-23-1.png)<!-- -->

``` r
# Plot eigenvalues of PCCoA analysis 
pccoa_eig <- pcoa_results$eig

barplot(pccoa_eig,
        main = "Eigenvalues of PCCoA analysis",
        xlab = "Eigenvalue number",
        ylab = "Eigenvalue")
```

![](Visualization_of_metabolomics_results_files/figure-gfm/unnamed-chunk-24-1.png)<!-- -->

\#Compare principal coordinate analysis with principal component
analysis.

``` r
# Perform PCA (principal components analysis)
species.pca <- rda(t(compound_Area_list))
```

Plot principal components and loadings plot. More on PCA components and
loadings you can read, for example, here:
<https://towardsdatascience.com/what-are-pca-loadings-and-biplots-9a7897f2e559>

``` r
# Perform PCA (principal components analysis)
biplot(species.pca)
```

![](Visualization_of_metabolomics_results_files/figure-gfm/unnamed-chunk-26-1.png)<!-- -->

Display summary of PCA analysis.

``` r
# Summary pf PCA results
summary(species.pca)
```

    ## 
    ## Call:
    ## rda(X = t(compound_Area_list)) 
    ## 
    ## Partitioning of variance:
    ##                 Inertia Proportion
    ## Total         9.857e+16          1
    ## Unconstrained 9.857e+16          1
    ## 
    ## Eigenvalues, and their contribution to the variance 
    ## 
    ## Importance of components:
    ##                             PC1       PC2       PC3       PC4       PC5
    ## Eigenvalue            8.833e+16 8.250e+15 1.864e+15 9.330e+13 2.804e+13
    ## Proportion Explained  8.961e-01 8.370e-02 1.891e-02 9.465e-04 2.845e-04
    ## Cumulative Proportion 8.961e-01 9.798e-01 9.987e-01 9.997e-01 1.000e+00
    ##                             PC6       PC7
    ## Eigenvalue            2.610e+12 4.753e+11
    ## Proportion Explained  2.647e-05 4.822e-06
    ## Cumulative Proportion 1.000e+00 1.000e+00
    ## 
    ## Scaling 2 for species and site scores
    ## * Species are scaled proportional to eigenvalues
    ## * Sites are unscaled: weighted dispersion equal on all dimensions
    ## * General scaling constant of scores:  39620.21 
    ## 
    ## 
    ## Species scores
    ## 
    ##                                                      PC1      PC2      PC3
    ## Butanedioic acid, 2TMS derivative                 2305.5  -206.41  1075.35
    ## Citric acid, 4TMS derivative                     -8765.1 -5692.81  4449.58
    ## D-Lactose                                        -7173.4 -9391.79 -2881.71
    ## Ethyl .alpha.-D-glucopyranoside, 4TMS derivative   890.5  -100.27   260.63
    ## Glycerol, 3TMS derivative                         2140.6  -327.99   426.06
    ## L-Proline, 2TMS derivative                         126.2   -22.26    32.52
    ## Lactic acid, 2TMS derivative                     35605.0 -3257.95   412.92
    ##                                                      PC4       PC5       PC6
    ## Butanedioic acid, 2TMS derivative                 873.50 -430.8522   33.7899
    ## Citric acid, 4TMS derivative                     -207.25   31.6775   -3.9196
    ## D-Lactose                                         124.16  -26.1315    3.8028
    ## Ethyl .alpha.-D-glucopyranoside, 4TMS derivative  329.10  350.8646  161.8391
    ## Glycerol, 3TMS derivative                         732.14  367.8547 -116.2757
    ## L-Proline, 2TMS derivative                        -44.52  -29.6787  -25.9833
    ## Lactic acid, 2TMS derivative                     -134.66   -0.3545    0.6482
    ## 
    ## 
    ## Site scores (weighted sums of species scores)
    ## 
    ##                   PC1      PC2       PC3      PC4     PC5      PC6
    ## Milk_Media1   -9592.4  -6870.6   6266.98   -221.9  2810.8   1287.6
    ## Kefir_T0_R1  -10269.6 -13657.8  10687.99  -1504.1  1861.9  -1340.1
    ## Kefir_T3_R1   -2023.5  -9494.5    405.68  -5817.9 -6600.6    396.6
    ## Kefir_T15_R1    941.1  -4687.3  -7644.02   2751.8 -7757.2   1054.6
    ## Kefir_T20_R1   3194.5  -5145.8  -6616.66   3884.2 -5085.6  -2639.6
    ## Kefir_T24_R1   4379.3  -2273.2  -5947.43   4197.8 -6038.6 -12745.3
    ## Kefir_T90_R1  19394.8  -5343.5  12167.02   3895.2  9378.6   4485.3
    ## Milk_Media2   -9525.4  -6748.6   2560.91   3181.2   868.8   4017.1
    ## Kefir_T0_R2   -8717.6   2053.8   2010.11   2105.1  1243.6   7156.6
    ## Kefir_T3_R2   -2923.8   2055.9  -5917.14   1509.0 -5958.1   5506.9
    ## Kefir_T15_R2   -609.6   3869.4  -8769.83   3336.0 -4517.5   9972.0
    ## Kefir_T20_R2   2756.4   3073.7  -6155.70   1319.2 -3940.7  -5296.0
    ## Kefir_T24_R2   4450.9    224.4  -8334.85   3640.9 -7046.5  -6863.6
    ## Kefir_T90_R2  17516.0  -2277.8  11929.57  -8137.0 -3284.1  11404.5
    ## Milk_Media3   -9940.8 -10449.7   7633.74  -1568.7  3271.1    648.0
    ## Kefir_T0_R3   -8661.2   2771.5   2918.53   2435.7  2764.7   1938.9
    ## Kefir_T3_R3   -8057.7   8228.6  -2425.72  13557.3 -2200.6  -1645.1
    ## Kefir_T15_R3    299.2  -2033.5  -2192.57  -4210.3 -3448.5   1108.3
    ## Kefir_T20_R3   4273.6   -192.7     58.02  -7337.9 -2062.9 -11644.5
    ## Kefir_T24_R3   6442.5   -618.4   -178.11  -7764.2 -3033.0 -17613.4
    ## Kefir_T3_R4   -7180.8  20184.4  11440.56  -1356.2  8380.7 -13208.3
    ## Kefir_T15_R4   1307.5  -1039.0   2505.49  -9384.9 -1110.4  -1507.1
    ## Kefir_T90_R4   8912.4   8592.5  10378.24  24397.0 -1477.8   4020.6
    ## Kefir_T90_R3   4493.5  -5672.4 -17841.49   2379.5 31891.4   -682.5
    ## Kefir_T20_R4   -378.2   7599.4 -10698.84  -8494.0 -3350.0  17407.2
    ## Kefir_T24_R4   -481.3  17851.3   1759.52 -16792.7  4440.4   4781.2

*Questions:* - What is the difference between principal component and
principal coordinate analysis? - What are the metabolites that
contribute most to PC1 and PC2 projections? - Why are some samples
located separately from others on the PCCoA and PCA plots?
