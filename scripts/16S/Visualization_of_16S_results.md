Visualization of 16Sresults
================
Eva Geissen
12/10/2022

## Learning Objectives

- Read in 16S abundance profiles
- Compare kefir microbial communities over time using ordination and
  other visualization methods
- Compare findings with comparable published data.

This R-markdown document loads csv file with the relative species
abundances that we produced in the last step of the NanoCLUST workflow,
adds metadata, and plots the relative species abundances over time. The
code in this notebook and the ausxiliary functions file was modified
from code provided by Eleonora Mastrorilli.

Open the file `Visualization_of_16S_results.Rmd` in RStudio to
interactively run through each code chunk by running the following
command *in terminal*:

``` bash
#conda activate metab && rstudio
```

To run a code chunk (grey boxes), click on the small green triangle in
the upper right corner of the box or use the keyboard short cut
Ctrl+Alt+C. For more details see the [Rmd
documentation](https://rmarkdown.rstudio.com/articles_intro.html).

### Packages

Load the required R packages

``` r
library(tidyverse)
```

    ## ── Attaching packages ─────────────────────────────────────── tidyverse 1.3.2 ──
    ## ✔ ggplot2 3.3.6      ✔ purrr   0.3.5 
    ## ✔ tibble  3.1.8      ✔ dplyr   1.0.10
    ## ✔ tidyr   1.2.1      ✔ stringr 1.4.1 
    ## ✔ readr   2.1.3      ✔ forcats 0.5.2 
    ## ── Conflicts ────────────────────────────────────────── tidyverse_conflicts() ──
    ## ✖ dplyr::filter() masks stats::filter()
    ## ✖ dplyr::lag()    masks stats::lag()

``` r
library(xlsx)
library(stringr)
library(data.table)
```

    ## 
    ## Attaching package: 'data.table'
    ## 
    ## The following objects are masked from 'package:dplyr':
    ## 
    ##     between, first, last
    ## 
    ## The following object is masked from 'package:purrr':
    ## 
    ##     transpose

``` r
library(ggplot2)
library(coin)
```

    ## Loading required package: survival

``` r
library(vegan)
```

    ## Loading required package: permute
    ## Loading required package: lattice
    ## This is vegan 2.6-4

### Specify paths and source the auxiliary functions

First we need to set path variables for your working directory and the
location of the NanoCLUST output and the metadata. The path given, is
the path to the data produced in the test run. We will provide you with
the path to your own data to replace it.

``` r
# set path.
# Make sure to set this path so that it correctly captures your home environment:
#my_root = "${HOME}/emvo_mcd22"
# To find out what your home directory is, switch back to the terminal and enter the following command
#"echo $HOME"

my_root = "/home/training15/embo_mcd22"
```

``` r
#setwd(my_root)

raw_data_path <- file.path(my_root,"data/NanoClust_output/combined_full2.csv")
metadata_path <- file.path(my_root,"scripts/16S")


source(file.path(metadata_path, "auxiliary_functions.R"), local = knitr::knit_global())
```

### Data wrangling

Before we can plot the information in the NanoCLUST output, we need to
wrangle the data a bit to get them in the needed shape. For example, we
will filter only for species level information and add some metadata
regarding the time points and replicate identities.

First, we read the csv file from the path where it is located and write
the content into a data frame.

``` r
# read 
#data_df <- data.frame()
#for(i in list.files(raw_data_path)){
#  data_df <- rbind.data.frame(read_csv(file.path(raw_data_path,i)))
#}
data_df <- read_csv(file.path(raw_data_path))
print(head(data_df))
```

    ## # A tibble: 6 × 4
    ##   sample                                         tax   taxid            rel_ab…¹
    ##   <chr>                                          <chr> <chr>               <dbl>
    ## 1 rel_abundance_FAT84254_pass_barcode01_5d0a93a5 F     Acetobacteraceae  0.00914
    ## 2 rel_abundance_FAT84254_pass_barcode01_5d0a93a5 F     Lactobacillaceae  0.991  
    ## 3 rel_abundance_FAT84254_pass_barcode02_5d0a93a5 F     Acetobacteraceae  0.0123 
    ## 4 rel_abundance_FAT84254_pass_barcode02_5d0a93a5 F     Lactobacillaceae  0.988  
    ## 5 rel_abundance_FAT84254_pass_barcode03_5d0a93a5 F     Acetobacteraceae  0.0255 
    ## 6 rel_abundance_FAT84254_pass_barcode03_5d0a93a5 F     Lactobacillaceae  0.975  
    ## # … with abbreviated variable name ¹​rel_abundance

Next, we extract the information about sample identity (the barcode
number) contained in the column `sample` and add an additional column
with this information.

``` r
# obtain useful info from Sample name
data_df <- data_df %>% 
  mutate(barcode = str_extract(sample,pattern = "barcode[0-9]{2}"))
```

The NanoCLUST output file contains information about the relative
abundance on all taxonomic levels, but we are only interested in the
species level. We filter the column `tax` in our data frame for ‘S’
which indicates species level to get data for the species level only.

``` r
filtered_df <- data_df[data_df$tax == 'S',]
```

Now we add some additional information, e.g. which barcode corresponds
to which time point and which replicate. We load a file that contains
this information and join the two data frames by their common column
`barcode`.

``` r
# load metadata xlsx
metadata_df <- read.xlsx(file.path(metadata_path,'timepoints_map.xlsx'),sheetIndex = 1)

joined_df <- right_join(filtered_df, metadata_df, by = 'barcode')
joined_df <- joined_df %>%
  mutate(time = as.numeric(time),
         replicate = as.factor(replicate))

my_df <- joined_df
#setDT(my_df)
```

Tidy up data and also explicitly define 0s.

``` r
my_df <- my_df %>% 
  ungroup() %>%
  select(replicate, time, taxid, rel_abundance) %>%
  complete(taxid, replicate, time) %>%
  mutate(rel_abundance = ifelse(is.na(rel_abundance), 0, rel_abundance))
```

Now the data is in the variable `my_df` and ready for plotting. Have a
look at it below.

``` r
my_df
```

    ## # A tibble: 60 × 4
    ##    taxid               replicate  time rel_abundance
    ##    <chr>               <fct>     <dbl>         <dbl>
    ##  1 Acetobacter fabarum 1             0       0.00914
    ##  2 Acetobacter fabarum 1             5       0.0255 
    ##  3 Acetobacter fabarum 1            15       0.0351 
    ##  4 Acetobacter fabarum 1            20       0.0230 
    ##  5 Acetobacter fabarum 1            24       0.0477 
    ##  6 Acetobacter fabarum 2             0       0.0123 
    ##  7 Acetobacter fabarum 2             5       0.0489 
    ##  8 Acetobacter fabarum 2            15       0.0350 
    ##  9 Acetobacter fabarum 2            20       0.0292 
    ## 10 Acetobacter fabarum 2            24       0.0376 
    ## # … with 50 more rows

### Global comparison of communities using ordination techniques

Ordination techniques are a family of statistical methods that allow
visual analysis of high-dimensional data. Specifically, they take
high-dimensional data and project data points into a lower-dimensional
space - typically 2D so that we can look at it.

Below we will compute a commonly used ordination - a Principle Component
Analysis (PCoA) - that takes as input a pairwise distance matrix between
samples and projects them into 2 Dimensions in such a way that the real
distances between all pairs are as little distorted as possible

``` r
my_df_wide <- my_df %>% 
  # combine time and replicate info into one column for now so that yo ucan extract info back out later.
  mutate(tmpSampleID = map2_chr(replicate, time, function(a, b) str_c(a, b, sep = "__"))) %>%
  pivot_wider(id_cols = tmpSampleID, names_from = taxid, values_from = rel_abundance) %>%
  as.data.frame() %>%
  column_to_rownames('tmpSampleID')
  #select(-replicate, -time) %>%
  #relocate(tmpSampleID)

print(my_df_wide)
```

    ##       Acetobacter fabarum Ameyamaea chiangmaiensis
    ## 1__0          0.009144159              0.000000000
    ## 1__5          0.025467960              0.000000000
    ## 1__15         0.035124816              0.000000000
    ## 1__20         0.022989691              0.000000000
    ## 1__24         0.047742496              0.000000000
    ## 2__0          0.012289839              0.000000000
    ## 2__5          0.048923679              0.001250832
    ## 2__15         0.035007290              0.000000000
    ## 2__20         0.029199615              0.000000000
    ## 2__24         0.037599208              0.000000000
    ##       Lactobacillus kefiranofaciens Lentilactobacillus kefiri
    ## 1__0                      0.8467370                 0.1441188
    ## 1__5                      0.7959827                 0.1785493
    ## 1__15                     0.6584288                 0.3064464
    ## 1__20                     0.6390893                 0.3362715
    ## 1__24                     0.5100638                 0.4382326
    ## 2__0                      0.8359206                 0.1517896
    ## 2__5                      0.7995844                 0.1502411
    ## 2__15                     0.7043790                 0.2606137
    ## 2__20                     0.6157432                 0.3537615
    ## 2__24                     0.5016036                 0.4580925
    ##       Paucilactobacillus hokkaidonensis Rhodovastum atsumiense
    ## 1__0                        0.000000000            0.000000000
    ## 1__5                        0.000000000            0.000000000
    ## 1__15                       0.000000000            0.000000000
    ## 1__20                       0.001649485            0.000000000
    ## 1__24                       0.002028855            0.001932243
    ## 2__0                        0.000000000            0.000000000
    ## 2__5                        0.000000000            0.000000000
    ## 2__15                       0.000000000            0.000000000
    ## 2__20                       0.000000000            0.001295683
    ## 2__24                       0.001226704            0.001477956

``` r
set.seed(2)
pairwiseDistances <- vegan::vegdist(my_df_wide, method = "manhattan")
pcoa <- cmdscale(pairwiseDistances, k = 2) 
colnames(pcoa) <- c("PCo 1", "PCo 2")
pcoa <- as.data.frame(pcoa)
pcoa$replicate <- map_chr(rownames(pcoa), function(x) str_split(x, "__")[[1]][1])
pcoa$time <- map_chr(rownames(pcoa), function(x) str_split(x, "__")[[1]][2])
pcoa$time <- factor(pcoa$time, levels = c(0, 5, 15, 20, 24))

#options(repr.plot.width=3.5, repr.plot.height=3.5)
ordinationplot <- pcoa %>%
  ggplot(aes(x = `PCo 1`, y = `PCo 2`)) +
  geom_point(aes(color = time, shape = replicate)) +
  theme_classic()
ordinationplot
```

![](Visualization_of_16S_results_files/figure-gfm/unnamed-chunk-12-1.png)<!-- -->

Samples taken at time point zero are all the way to the right and the
further we go in time the more the samples shift to the left. This shows
that there is global differences in the kefir community that are
correlated well with the time and which we will explore in a bit more
detail moving forward.

Furthermore, this plot suggests that the difference between technical
replicates - the technical variation - are fairly limited, except
between replicates of the time point after 5 hours. This is something we
will further explore when looking into the taxonomic composition in more
detail.

### Plotting of relative species abundance time courses

Next, to get a more in-depth understanding of the community dynamics in
Kefir ofer time, we will plot the time course of the relative abundance
for each species as well as the individual measurements (replicates) at
each time point.

``` r
# one plot for each species 
#for (d in levels(as.factor(my_df$taxid))){

my_df <- my_df %>%
  rename(species = taxid)

tmp <- my_df %>% 
  group_by(species, time) %>% summarize(rel_abundance_mean = mean(rel_abundance),
                               s = sd(rel_abundance)) %>%
  # for boxplot, if we want to show
  mutate(ymax = rel_abundance_mean + s,
         ymin = rel_abundance_mean - s)
```

    ## `summarise()` has grouped output by 'species'. You can override using the
    ## `.groups` argument.

``` r
speciesOrder <- my_df %>%
  group_by(species) %>%
  summarize(meanAbundance = mean(rel_abundance)) %>%
  arrange(desc(meanAbundance)) %>%
  pull(species)

tmp$species <- factor(tmp$species, levels = speciesOrder)


p <- ggplot() +
        geom_line(data = tmp, aes(x = time, y = rel_abundance_mean, color = species, group = species)) +
        #geom_pointrange(data = tmp, aes(x = time, y = rel_abundance_mean, ymax = ymax, ymin = ymin, color = species)) +
        geom_point(data = my_df, aes(x = time, y = rel_abundance, color = species), alpha = 0.4) +
        #scale_y_log10()+
        xlim(0, 25)+
        ylim(-0.02, 1) +
  theme_classic() +
  ylab("Bacterial relative abundance") +
  xlab("Time (h)") +
  scale_x_continuous(breaks = c(0, 5, 15, 20, 24))
```

    ## Scale for 'x' is already present. Adding another scale for 'x', which will
    ## replace the existing scale.

``` r
#} 
print(p)
```

![](Visualization_of_16S_results_files/figure-gfm/unnamed-chunk-13-1.png)<!-- -->

This plots suggests that - The kefir community is dominated by two
species Lactobacillus kefiranofaciens and Lentilactobacillus kefiri;
Acetobacter fabarum is lowly abundant but (more or less) consistently
detectable over time. It seems to increase in abundance particularly
after 24 hours. - In terms of relative abundance, L. kefiranofaciens
consistently drops over time while L. kefiri rises over time. Keep in
mind that our data is compositional, which means that (strong) increases
in abundance of one species will have an effect of the (relative)
abundance of the entire community.

Next, let’s compare our analysis to a similar plot from Blasche et
al. (Figure 1D here:
<https://www.nature.com/articles/s41564-020-00816-5>)

Our analysis conforms with Blasche et al. in that L. kefiranofaciens
dominates consistently and that A. fabarum seems to become more
prominent towards the end of the time course. In contrast, the authors
report many other species that we do not detect, some of which are
fairly abundant in the community, for example Lactococcus lactis.

What could be the reasons for this discrepancy?

``` r
gg_color_hue <- function(n) {
  hues = seq(15, 375, length = n + 1)
  hcl(h = hues, l = 65, c = 100)[1:n]
}

# The following line controls the plot size in a R jupyter notebook :)
#options(repr.plot.width=4, repr.plot.height=2.5)
barplot <- my_df %>%
  ggplot() +
  geom_bar(aes(x = replicate, y = rel_abundance, fill = species), stat='identity') +
  theme_bw() +
  #scale_fill_manual(values = c(gg_color_hue(length(unique(my_df$species)) - 1), "#808080")) +
  theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
  facet_grid(~time) +
  ylab("Relative abundance")
  NULL
```

    ## NULL

``` r
barplot
```

![](Visualization_of_16S_results_files/figure-gfm/unnamed-chunk-14-1.png)<!-- -->

The between-replicate variability seems to be somehow increased in that
Acetobacter fabarum is detected in only one of the two replicates. The
difference seems marginal, and the offset of replicate 2 in the
ordination seems to be an ordination-artifact more than reflecting true
variation in the data.

## Take-home messages

- Kefir is home to a relatively simple and well-defined microbial
  community of only a few bacterial species
- There are pronounced and continuous community shifts in the kefir
  community that correlate well with time.
- Microbial communites are compositional in nature, which means that
  abundance changes in one species will inversely affect the relative
  abundance of other species - particularly if the changing species is
  highly abundant. Without data augmentation, it is typically not
  possible to understand the exact nature of the community shift.
- Sequencing technologies as well as the region of the 16S gene that is
  sequenced can affect

## Further steps (in case you’re fast)

- The line plot of species abundances over time suggests that temporal
  variability dominates over technical variability.
  - What does this mean?
  - Is this encouraging or discouraging?
  - What are possible ways to quantify and compare these two sources of
    variability?
